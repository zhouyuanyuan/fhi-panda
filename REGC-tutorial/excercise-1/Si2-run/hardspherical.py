#!/usr/bin/python
import sys
import copy
import math 
import random
import os
import config
# Denfine some classes
# Define an atom class
class Atom:
    x = y = z = 0.0
    eps = sig = 0.0
    mass = 0.0

# Define a configuration class - configuration is a collection of atoms
class Configuration:
    def __init__(self,na):
        self.natoms = na
        self.COM = [0.0,0.0,0.0]
        self.atom = []
        for i in xrange(0,na):
            self.atom.append(Atom())

# Calculate Center Of Mass
    def CalcCOM(self):
        M = 0.0
        sumx = sumy = sumz = 0.0
        for i in xrange(0, self.natoms):
            m = self.atom[i].mass
            sumx += self.atom[i].x*m
            sumy += self.atom[i].y*m
            sumz += self.atom[i].z*m
            M    += m
        Cx = sumx/M
        Cy = sumy/M
        Cz = sumz/M
        self.COM[0] = Cx
        self.COM[1] = Cy
        self.COM[2] = Cz  
        return self.COM
    def RadGyr(self):
        sumgyr = 0.0
        M = 0.0
        self.CalcCOM()
        comx = self.COM[0]
        comy = self.COM[1]
        comz = self.COM[2]
        for i in xrange():
            M += self.atom[i].mass
            mc = self.atom[i].mass
            rx = self.atom[i].x
            ry = self.atom[i].y
            rz = self.atom[i].z
            sgx = (rx-comx)**2.0
            sgy = (ry-comy)**2.0
            sgz = (rz-comz)**2.0
        
            sumgyr += mc*(sgx+sgy+sgz)
        Rsq = sumgyr / M
        R   = math.sqrt(Rsq)
        return R

        return 0.0

# Statisticis Average
class AverStat:
    n = 0
    Mold = Mnew = Sold = Snew =0.0
    def Push(self, val):
        self.n += 1
        if self.n == 1:
            self.Mold = val
            self.Sold = 0.0
        else:
            self.Mnew = self.Mold + (val-self.Mold)/(float(self.n));
            self.Snew = self.Sold + (val-self.Mold)*(val-self.Mnew);
            self.Mold = self.Mnew;
            self.Sold = self.Snew;
    def Mean(self):
        if self.n > 0:
            return self.Mnew
        else:
            return 0.0
    def Variance(self):
        if self.n > 1:
            vary = self.Snew/(float(self.n)-1.0)
            return vary
        else:
            return 0.0
    def Deviation(self):
        dev = sqrt(self.Variance())
        return dev
    def Reset():
        self.n = 0

## Hard sphere to make sure that the atoms inside the sphere and avoid overlaping between atoms 
class HardSphericalWall:
    def __init__(self,coord,cent):
        self.atom   = coord
        self.center = cent
    def CheckIFInside(self,number):
        itag = 1
        dst_com = (self.center[0]-self.atom[number][0])**2 +  (self.center[1]-self.atom[number][1])**2 +  (self.center[2]-self.atom[number][2])**2
        if  0.0<dst_com<config.radius**2:
            return itag
        else:
            itag=0
        return itag
    def overlapd(self,number):
        tag = 1
        n = len(self.atom)
        if(number < config.n_metal):
            for i in range(0,number):
                dst1 = (self.atom[i][0]-self.atom[number][0])**2 +  (self.atom[i][1]-self.atom[number][1])**2 +  (self.atom[i][2]-self.atom[number][2])**2
                if (dst1<config.dist[0][0]**2):
                    tag = 0
                else: continue
            for i in range(number+1,config.n_metal):
                dst1 = (self.atom[i][0]-self.atom[number][0])**2 +  (self.atom[i][1]-self.atom[number][1])**2 +  (self.atom[i][2]-self.atom[number][2])**2
                if (dst1<config.dist[0][0]**2):
                    tag = 0 
                else: continue
            for i in range(config.n_metal,n):
                dst1 = (self.atom[i][0]-self.atom[number][0])**2 +  (self.atom[i][1]-self.atom[number][1])**2 +  (self.atom[i][2]-self.atom[number][2])**2
                if (dst1<config.dist[0][1]**2):
                                    tag = 0
                else: continue
        else:
            for i in range(0,config.n_metal):
                dst1 = (self.atom[i][0]-self.atom[number][0])**2 +  (self.atom[i][1]-self.atom[number][1])**2 +  (self.atom[i][2]-self.atom[number][2])**2
                if (dst1<config.dist[0][1]**2):
                                        tag = 0
                else: continue
            for i in range(config.n_metal,number):
                dst1 = (self.atom[i][0]-self.atom[number][0])**2 +  (self.atom[i][1]-self.atom[number][1])**2 +  (self.atom[i][2]-self.atom[number][2])**2
                if (dst1<config.dist[1][1]**2):
                                    tag = 0 
                else: continue
            for i in range(number+1,n):
                dst1 = (self.atom[i][0]-self.atom[number][0])**2 +  (self.atom[i][1]-self.atom[number][1])**2 +  (self.atom[i][2]-self.atom[number][2])**2
                if (dst1<config.dist[1][1]**2):
                                        tag = 0
                else: continue
        return tag

    def overlapa(self,number):
        tag = 1
        n = len(self.atom)
        for i in range(0,config.n_metal):
            dst1 = (self.atom[i][0]-self.atom[number][0])**2 +  (self.atom[i][1]-self.atom[number][1])**2 +  (self.atom[i][2]-self.atom[number][2])**2
            if (dst1<config.dist[0][1]**2):
                            tag = 0
            else: continue
        for i in range(config.n_metal,n-1):
            dst1 = (self.atom[i][0]-self.atom[number][0])**2 +  (self.atom[i][1]-self.atom[number][1])**2 +  (self.atom[i][2]-self.atom[number][2])**2
            if (dst1<config.dist[1][1]**2):
                            tag = 0
            else: continue
        return tag

# Hard Cubix Box 
class HardCubicBox:
        def __init__(self, cent, l):
                self.center = cent
                self.bx = l
                self.by = l
                self.bz = l
                #lower box bounds
                self.bxl = self.center[0]-self.bx/2.0
                self.byl = self.center[1]-self.by/2.0
                self.bzl = self.center[2]-self.bz/2.0
                #upper box bounds
                self.bxh = self.center[0]+self.bx/2.0
                self.byh = self.center[1]+self.by/2.0
                self.bzh = self.center[2]+self.bz/2.0
                return

        def CheckIfInside(self, x, y, z):
                dx = x - self.center[0]
                dy = y - self.center[1]
                dz = z - self.center[2]
                #x bound
                if dx<self.bxl or dx>self.bxh :
                        return 0
                #y bound
                elif dy<self.byl or dy>self.byh :
                        return 0
                #z bound
                elif dz<self.bzl or dz>self.bzh :
                        return 0
                else:
                        return 1

        def ComputeVolume(self):
                return self.bx*self.by*self.bz

        def SetBox(self, l):
                self.bx = l
                self.by = l
                self.bz = l
                #lower box bounds
                self.bxl = self.center[0]-self.bx/2.0
                self.byl = self.center[1]-self.by/2.0
                self.bzl = self.center[2]-self.bz/2.0
                #upper box bounds
                self.bxh = self.center[0]+self.bx/2.0
                self.byh = self.center[1]+self.by/2.0
                self.bzh = self.center[2]+self.bz/2.0
                return
        def ResetSizebyVolume(self, Vol):
                l = Vol**(1.0/3.0)
                self.bx = l
                self.by = l
                self.bz = l
                #lower box bounds
                self.bxl = self.center[0]-self.bx/2.0
                self.byl = self.center[1]-self.by/2.0
                self.bzl = self.center[2]-self.bz/2.0
                #upper box bounds
                self.bxh = self.center[0]+self.bx/2.0
                self.byh = self.center[1]+self.by/2.0
                self.bzh = self.center[2]+self.bz/2.0

        def PrintDetails(self):
                print("Hard Cubic Box Boundary")
                print("Cubic Box Size: ", self.bx, " center: ", self.center)
                print("Volume: ", self.ComputeVolume())
                return
