import sys
import os
import copy
import config
import random
import math
from hardspherical import *
import numpy as np
from tool import *

def mcexch(coord, velocity, temp):
    coord_new_exch = np.copy(coord)
    veloc_new_exch = np.copy(velocity)
    num_particle   = len(coord_new_exch)
    num_adsorbate  = num_particle - config.n_metal
    if(num_adsorbate>0):
                rand = random.random()
                if(rand < 0.5):
                        dimer_list = []
                        dist_list  = []
                        if(2>num_adsorbate >0):
                                pass
                        elif(num_adsorbate>=2):
                                for i in range(config.n_metal, num_particle-1, 1):
                                        dimer_tmp = []
                                        dis_dimer = np.sqrt(np.sum(np.square(np.subtract(coord_new_exch[i], coord_new_exch[i+1]))))
                                        if dis_dimer < 1.5*config.dist[1][1]:
                                                dimer_tmp.append(i)
                                                dimer_tmp.append(i+1)
                                                dimer_list.append(dimer_tmp)
                        dist_list.append(dis_dimer)
                        if(len(dimer_list)==0):
                                k =  int(num_adsorbate*random.random())+config.n_metal
                                coord_new_exch     = np.delete(coord_new_exch, k, 0)
                                veloc_new_exch     = np.delete(veloc_new_exch, k, 0)
                                coord              = np.copy(coord_new_exch)
                                velocity           = np.copy(veloc_new_exch)

                        elif(len(dimer_list)>0):
                                r_rand = random.random()
                                if(r_rand < 0.5):
# for each replica, we attempt remove an oxygen
                                        k =  int(num_adsorbate*random.random())+config.n_metal
                                        coord_new_exch     = np.delete(coord_new_exch, k, 0)
                                        veloc_new_exch     = np.delete(veloc_new_exch, k, 0)
                                        coord              = np.copy(coord_new_exch)
                                        velocity           = np.copy(veloc_new_exch)

                        else:
# try to delete the H2 molecule with the shortest distance

                            dist_min     = min(dist_list)
                            index_dimer  = dist_list.index(dist_min)

#                                        index_dimer = int(len(dimer_list)*random.random())
                            coord_new_exch     = np.delete(coord_new_exch, dimer_list[index_dimer][0], 0)
                            coord_new_exch     = np.delete(coord_new_exch, dimer_list[index_dimer][1]-1, 0)
                            veloc_new_exch     = np.delete(veloc_new_exch, dimer_list[index_dimer][0], 0)
                            veloc_new_exch     = np.delete(veloc_new_exch, dimer_list[index_dimer][1]-1, 0)
                            coord              = np.copy(coord_new_exch)
                            velocity           = np.copy(veloc_new_exch)

########################################################################################################################################################

#Add a particle
                else:
                        a_rand = random.random()
                        if(a_rand<0.5):
#                                c = Configuration(num_particle)
 #                               for index in xrange(num_particle):
  #                                      if index < config.n_metal:
   #                                             c.atom[index].mass = config.mass[0]
    #                                    else:
      #                                          c.atom[index].mass = config.mass[1]
       #                                 c.atom[index].x = coord_new_exch[index][0]
        #                                c.atom[index].y = coord_new_exch[index][1]
         #                               c.atom[index].z = coord_new_exch[index][2]
          #                      COM   = c.CalcCOM()
                            COM = [0.0, 0.0, 0.0]
                            for index in xrange(num_particle):
                                COM[0] = COM[0] + coord_new_exch[index][0]
                                COM[1] = COM[1] + coord_new_exch[index][1]
                                COM[2] = COM[2] + coord_new_exch[index][2]
                            COM[0] = COM[0]/float(num_particle)
                            COM[1] = COM[1]/float(num_particle)
                            COM[2] = COM[2]/float(num_particle)


                            com_radius = config.radius*random.random()
                            theta=random.random()*config.pi
                            phi=random.random()*2.0*config.pi

                            x_new = com_radius*math.sin(theta)*math.cos(phi) + COM[0]
                            y_new = com_radius*math.sin(theta)*math.sin(phi) + COM[1]
                            z_new = com_radius*math.cos(theta) + COM[2]
                            coord_new_exch = np.append(coord_new_exch, [x_new,y_new,z_new])
                            coord_new_exch = np.reshape(coord_new_exch, (-1,3))
                            boundary = HardSphericalWall(coord_new_exch,COM)
                            in_tag1 = boundary.CheckIFInside(-1)
                            if (in_tag1 == 1):
                                    atag1 = boundary.overlapa(-1)
                                    if (atag1 == 1):
                                            coord = np.copy(coord_new_exch)

                        else:
#                                c = Configuration(num_particle)
 #                               for index in xrange(num_particle):
  #                                      if index < config.n_metal:
   #                                             c.atom[index].mass = config.mass[0]
    #                                    else:
     #                                           c.atom[index].mass = config.mass[1]
#                                       c.atom[index].x = coord_new_exch[index][0]
 #                                          c.atom[index].y = coord_new_exch[index][1]
  #                                         c.atom[index].z = coord_new_exch[index][2]
   #                             COM   = c.CalcCOM()
                            COM = [0.0, 0.0, 0.0]
                            for index in xrange(num_particle):
                                COM[0] = COM[0] + coord_new_exch[index][0]
                                COM[1] = COM[1] + coord_new_exch[index][1]
                                COM[2] = COM[2] + coord_new_exch[index][2]
                            COM[0] = COM[0]/float(num_particle)
                            COM[1] = COM[1]/float(num_particle)
                            COM[2] = COM[2]/float(num_particle)

                
                            com_radius = config.radius*random.random()
                            theta=random.random()*config.pi
                            phi=random.random()*2.0*config.pi

                            H2_comx = com_radius*math.sin(theta)*math.cos(phi) + COM[0]
                            H2_comy = com_radius*math.sin(theta)*math.sin(phi) + COM[1]
                            H2_comz = com_radius*math.cos(theta) + COM[2]


                            theta=random.random()*config.pi
                            phi=random.random()*2.0*config.pi
                            x1_new = 0.5*config.dist[1][1]*math.sin(theta)*math.cos(phi) + H2_comx
                            y1_new = 0.5*config.dist[1][1]*math.sin(theta)*math.sin(phi) + H2_comy
                            z1_new = 0.5*config.dist[1][1]*math.cos(theta) + H2_comz
                

                            x2_new = 0.5*config.dist[1][1]*math.sin(config.pi-theta)*math.cos(config.pi+phi) + H2_comx
                            y2_new = 0.5*config.dist[1][1]*math.sin(config.pi-theta)*math.sin(config.pi+phi) + H2_comy
                            z2_new = 0.5*config.dist[1][1]*math.cos(config.pi-theta) + H2_comz


                            dist_H2=math.sqrt((x1_new-x2_new)**2+(y1_new-y2_new)**2+(z1_new-z2_new)**2)


                            coord_new_exch = np.append(coord_new_exch, [x1_new,y1_new,z1_new])
                            coord_new_exch = np.reshape(coord_new_exch, (-1,3))
                            boundary = HardSphericalWall(coord_new_exch,COM)
                            in_tag1 = boundary.CheckIFInside(-1)

                            if (in_tag1 == 1):
                                    atag1 = boundary.overlapa(-1)


                                    if (atag1 == 1):
                                            coord_new_exch = np.append(coord_new_exch, [x2_new,y2_new,z2_new])
                                            coord_new_exch = np.reshape(coord_new_exch, (-1,3))
                                            boundary = HardSphericalWall(coord_new_exch,COM)
                                            in_tag2  = boundary.CheckIFInside(-1)

                                            if (in_tag2 == 1):
                                                    atag2 = boundary.overlapa(-1)
                                                    if (atag2 == 1):
                                                            coord = np.copy(coord_new_exch)


    else:
        a_rand = random.random()
        if(a_rand<0.5):
#                        c = Configuration(num_particle)
 #                       for index in xrange(num_particle):
  #                              if index < config.n_metal:
   #                                     c.atom[index].mass = config.mass[0]
    #                            else:
     #                                   c.atom[index].mass = config.mass[1]
      #                          c.atom[index].x = coord_new_exch[index][0]
#                                c.atom[index].y = coord_new_exch[index][1]
 #                               c.atom[index].z = coord_new_exch[index][2]
  #                      COM   = c.CalcCOM()
            COM = [0.0, 0.0, 0.0]
            for index in xrange(num_particle):
                COM[0] = COM[0] + coord_new_exch[index][0]
                COM[1] = COM[1] + coord_new_exch[index][1]
                COM[2] = COM[2] + coord_new_exch[index][2]
            COM[0] = COM[0]/float(num_particle)
            COM[1] = COM[1]/float(num_particle)
            COM[2] = COM[2]/float(num_particle)


            com_radius = config.radius*random.random()
            theta=random.random()*config.pi
            phi=random.random()*2.0*config.pi
            x_new = com_radius*math.sin(theta)*math.cos(phi) + COM[0]
            y_new = com_radius*math.sin(theta)*math.sin(phi) + COM[1]
            z_new = com_radius*math.cos(theta) + COM[2]
            coord_new_exch = np.append(coord_new_exch, [x_new,y_new,z_new])
            coord_new_exch = np.reshape(coord_new_exch, (-1,3))
            boundary = HardSphericalWall(coord_new_exch,COM)
            in_tag1 = boundary.CheckIFInside(-1)
            if (in_tag1 == 1):
                atag1 = boundary.overlapa(-1)
                if (atag1 == 1):
                    coord = np.copy(coord_new_exch)


        else:
#                        c = Configuration(num_particle)
 #                       for index in xrange(num_particle):
  #                              if index < config.n_metal:
   #                                     c.atom[index].mass = config.mass[0]
    #                            else:
     #                                   c.atom[index].mass = config.mass[1]
      #                          c.atom[index].x = coord_new_exch[index][0]
       #                         c.atom[index].y = coord_new_exch[index][1]
        #                        c.atom[index].z = coord_new_exch[index][2]
         #               COM   = c.CalcCOM()
            COM = [0.0, 0.0, 0.0]
            for index in xrange(num_particle):
                COM[0] = COM[0] + coord_new_exch[index][0]
                COM[1] = COM[1] + coord_new_exch[index][1]
                COM[2] = COM[2] + coord_new_exch[index][2]
            COM[0] = COM[0]/float(num_particle)
            COM[1] = COM[1]/float(num_particle)
            COM[2] = COM[2]/float(num_particle)

    
            com_radius = config.radius*random.random()
            theta=random.random()*config.pi
            phi=random.random()*2.0*config.pi

            H2_comx = com_radius*math.sin(theta)*math.cos(phi) + COM[0]
            H2_comy = com_radius*math.sin(theta)*math.sin(phi) + COM[1]
            H2_comz = com_radius*math.cos(theta) + COM[2]


            theta=random.random()*config.pi
            phi=random.random()*2.0*config.pi
            x1_new = 0.5*config.dist[1][1]*math.sin(theta)*math.cos(phi) + H2_comx
            y1_new = 0.5*config.dist[1][1]*math.sin(theta)*math.sin(phi) + H2_comy
            z1_new = 0.5*config.dist[1][1]*math.cos(theta) + H2_comz
            

            x2_new = 0.5*config.dist[1][1]*math.sin(config.pi-theta)*math.cos(config.pi+phi) + H2_comx
            y2_new = 0.5*config.dist[1][1]*math.sin(config.pi-theta)*math.sin(config.pi+phi) + H2_comy
            z2_new = 0.5*config.dist[1][1]*math.cos(config.pi-theta) + H2_comz

            dist_H2=math.sqrt((x1_new-x2_new)**2+(y1_new-y2_new)**2+(z1_new-z2_new)**2)



            coord_new_exch = np.append(coord_new_exch, [x1_new,y1_new,z1_new])
            coord_new_exch = np.reshape(coord_new_exch, (-1,3))
            boundary = HardSphericalWall(coord_new_exch,COM)
            in_tag1 = boundary.CheckIFInside(-1)

            if (in_tag1 == 1):
                    atag1 = boundary.overlapa(-1)
                    if (atag1 == 1):
                            atag2 = boundary.overlapa(-1)

                            if (atag2 == 1):
                                coord = np.copy(coord_new_exch)
    return coord, velocity

def accept_r(T, Mu, e_f, e_a, nummber):
    T  = float(T)
    beta = 1.0/(T*config.kB)
#########################################################################################
##       1 u/Da = 931MeV/(c)**2 = 931*1.0e6 eV/(c)**2                                  ##
##       lamada = math.sqrt(h**2/(2*pi*mass_O)*kb*T))*c*(1.0e3)/31.0                   ##
#########################################################################################
#       lamada = math.sqrt(h**2/(2*pi*mass[1])*beta)*c/(31.0*(1.0e3))
#########################################################################################
##    ---grand canonical coupled to an ideal gas bath with pressure: pid               ##
##        chemical potential bath: mu^b = mu^0 + ln(beta*pid)/beta                     ##
##                                      = ln (beta*pid*Lamda^3)/beta                   ##
##        zz is defined as         zz   = exp(beta*mu^b)/Lamda^3                       ##
##                                      = beta*pid                                     ##
##        excess chemical pot.     muex = mu^b -mu^id                                  ##
##                                      = mu^0 + ln(beta*pid)/beta - mu^0 - ln(rho)    ##
##                                      = ln(zz)/beta - ln <rho>                       ##
#########################################################################################      
#       ZZ = math.exp(beta*mu)/((lamada)**3)
#       ZZ = beta*pid
#       ZZ = cal_zz(Mu, T)
    num_adsorbate = nummber-config.n_metal
    lamd = cal_lamada(T)

    if (-beta*(e_a-e_f+config.EE+Mu)>709):
        pacc_r = 1.0
    else:
        pacc_r = (num_adsorbate*lamd**3)/(config.Vol)*(math.exp(-beta*(e_a-e_f+config.EE+Mu)))
    return pacc_r

def accept_r2(T, Mu, e_f, e_a, nummber):
    T  = float(T)
    beta = 1.0/(T*config.kB)
#########################################################################################
##       1 u/Da = 931MeV/(c)**2 = 931*1.0e6 eV/(c)**2                                  ##
##       lamada = math.sqrt(h**2/(2*pi*mass_O)*kb*T))*c*(1.0e3)/31.0                   ##
#########################################################################################
#       lamada = math.sqrt(h**2/(2*pi*mass[1])*beta)*c/(31.0*(1.0e3))
#########################################################################################
##    ---grand canonical coupled to an ideal gas bath with pressure: pid               ##
##        chemical potential bath: mu^b = mu^0 + ln(beta*pid)/beta                     ##
##                                      = ln (beta*pid*Lamda^3)/beta                   ##
##        zz is defined as         zz   = exp(beta*mu^b)/Lamda^3                       ##
##                                      = beta*pid                                     ##
##        excess chemical pot.     muex = mu^b -mu^id                                  ##
##                                      = mu^0 + ln(beta*pid)/beta - mu^0 - ln(rho)    ##
##                                      = ln(zz)/beta - ln <rho>                       ##
#########################################################################################      
#       ZZ = math.exp(beta*mu)/((lamada)**3)
#       ZZ = beta*pid
#       ZZ = cal_zz(Mu, T)
    num_adsorbate = nummber-config.n_metal
    lamd = cal_lamada(T)

    if (-beta*(e_a-e_f+2*config.EE+2*Mu)>709):
        pacc_r = 1.0
    else:
        pacc_r = (num_adsorbate*lamd**3)/(config.Vol)*(math.exp(-beta*(e_a-e_f+2*config.EE+2*Mu)))
    return pacc_r

def accept_a(T, Mu, e_f, e_a, nummber):
    T  = float(T)
    beta = 1.0/(T*config.kB)
#########################################################################################
##       1 u/Da = 931MeV/(c)**2 = 931*1.0e6 eV/(c)**2                                  ##
##       lamada = math.sqrt(h**2/(2*pi*mass_O)*kb*T))*c*(1.0e3)/31.0                   ##
#########################################################################################
#       lamada = math.sqrt(h**2/(2*pi*mass[1])*beta)*c/(31.0*(1.0e3))
#########################################################################################
##    ---grand canonical coupled to an ideal gas bath with pressure: pid               ##
##        chemical potential bath: mu^b = mu^0 + ln(beta*pid)/beta                     ##
##                                      = ln (beta*pid*Lamda^3)/beta                   ##
##        zz is defined as         zz   = exp(beta*mu^b)/Lamda^3                       ##
##                                      = beta*pid                                     ##
##        excess chemical pot.     muex = mu^b -mu^id                                  ##
##                                      = mu^0 + ln(beta*pid)/beta - mu^0 - ln(rho)    ##
##                                      = ln(zz)/beta - ln <rho>                       ##
#########################################################################################      
#       ZZ = math.exp(beta*mu)/((lamada)**3)
#       ZZ = beta*pid
#       ZZ = cal_zz(Mu, T)
    lamd = cal_lamada(T) 
    if (-beta*(e_a-e_f-config.EE-Mu)>709):
        pacc_a = 1.0
    else:
        pacc_a = config.Vol/((num_adsorbate + 1)*lamd**3)*(math.exp(-beta*(e_a-e_f-config.EE-Mu)))
    return pacc_a

def accept_a2(T, Mu, e_f, e_a, nummber):
    T  = float(T)
    beta = 1.0/(T*config.kB)
#########################################################################################
##       1 u/Da = 931MeV/(c)**2 = 931*1.0e6 eV/(c)**2                                  ##
##       lamada = math.sqrt(h**2/(2*pi*mass_O)*kb*T))*c*(1.0e3)/31.0                   ##
#########################################################################################
#       lamada = math.sqrt(h**2/(2*pi*mass[1])*beta)*c/(31.0*(1.0e3))
#########################################################################################
##    ---grand canonical coupled to an ideal gas bath with pressure: pid               ##
##        chemical potential bath: mu^b = mu^0 + ln(beta*pid)/beta                     ##
##                                      = ln (beta*pid*Lamda^3)/beta                   ##
##        zz is defined as         zz   = exp(beta*mu^b)/Lamda^3                       ##
##                                      = beta*pid                                     ##
##        excess chemical pot.     muex = mu^b -mu^id                                  ##
##                                      = mu^0 + ln(beta*pid)/beta - mu^0 - ln(rho)    ##
##                                      = ln(zz)/beta - ln <rho>                       ##
#########################################################################################      
#       ZZ = math.exp(beta*mu)/((lamada)**3)
#       ZZ = beta*pid
#       ZZ = cal_zz(Mu, T)
    lamd = cal_lamada(T)
    num_adsorbate = nummber-config.n_metal
    if (-beta*(e_a-e_f-2*config.EE-2*Mu)>709):
        pacc_a = 1.0
    else:
        pacc_a = config.Vol/((num_adsorbate + 2)*lamd**3)*(math.exp(-beta*(e_a-e_f-2*config.EE-2*Mu)))
    return pacc_a
