The FHI-panda user guide describes how to run and use the
various features of FHI-panda. This guide shows the capabilities of
the program, how to use these capabilities, the necessary input
files and formats, and how to run the program both on
uniprocessor machines and in parallel. FHI-panda is available free
of charge for non-commercial use by individuals, academic or
research institutions, upon the request an access from the web site
https://gitlab.com/zhouyuanyuan/fhi-panda.


##################REFERENCE#############################################################################

1 Y. Zhou, M. Scheffler, and L. M. Ghiringhelli, "Determining surface phase diagrams including anharmonic effects," Phys. Rev. B 100, 174106 (2019) <https://link.aps.org/pdf/10.1103/PhysRevB.100.174106>

2 Y. Zhou. Surface phase diagrams including anharmonic effects via a replica-exchange grand-canonical method. Doctoral thesis, Technische Universität Berlin, Berlin, 2020. <https://depositonce.tu-berlin.de/handle/11303/11723>


3 Y. Zhou, C. Zhu, M. Scheffler, and L. M. Ghiringhelli, "Ab initio approach for thermodynamic surface phases with full considerabtion of anharmonic effect -- the example of hydrogen at Si(100)", Phys. Rev. Lett 128, 246101 <https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.128.246101>
Here we will run a very small example of a $\mathrm{Si}_2$ cluster in a $\mathrm{H}_2$
gas phase to demonstrate the workflow of the REGC method.

#####################################################################################################


##############################Directories#####################

'src'  the latest version written by python3.X

'REGC-tutorial'  the example of Si2 cluster in the hydrogen gas phase: REGC simulation and the postprocessing to determine phase diagram

'src-python2' the version based on python2.7

'examples'    the tutorial of the python2.7 version

'doc'         the mannual

