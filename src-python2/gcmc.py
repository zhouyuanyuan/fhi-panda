import sys
import os
import copy
import config
import random
import math
from periodic_check import *
import numpy as np
from tool import *

def mcexch(coord, velocity, temp):
        coord_new_exch = np.copy(coord)
	veloc_new_exch = np.copy(velocity)
	num_particle   = len(coord_new_exch)
        num_adsorbate  = num_particle - config.n_metal
	if(num_adsorbate>0):
                rand = random.random()
                if(rand < 0.5):
                        dimer_list = []
			dist_list  = []
                        if(2>num_adsorbate >0):
                                pass
                        elif(num_adsorbate>=2):
                                for i in range(config.n_metal, num_particle-1, 1):
                                        dimer_tmp = []
                                        dis_dimer = np.sqrt(np.sum(np.square(np.subtract(coord_new_exch[i], coord_new_exch[i+1]))))
                                        if dis_dimer < 1.5*config.dist[1][1]:
                                                dimer_tmp.append(i)
                                                dimer_tmp.append(i+1)
                                                dimer_list.append(dimer_tmp)
						dist_list.append(dis_dimer)
                        if(len(dimer_list)==0):
                                k =  int(num_adsorbate*random.random())+config.n_metal
                                coord_new_exch     = np.delete(coord_new_exch, k, 0)
				veloc_new_exch     = np.delete(veloc_new_exch, k, 0)
                        	coord              = np.copy(coord_new_exch)
	                        velocity           = np.copy(veloc_new_exch)

                        elif(len(dimer_list)>0):
                                r_rand = random.random()
                                if(r_rand < 0.5):
# for each replica, we attempt remove an oxygen
                                        k =  int(num_adsorbate*random.random())+config.n_metal
                                        coord_new_exch     = np.delete(coord_new_exch, k, 0)
					veloc_new_exch     = np.delete(veloc_new_exch, k, 0)
					coord              = np.copy(coord_new_exch)
                                        velocity           = np.copy(veloc_new_exch)

                                else:
# try to delete the H2 molecule with the shortest distance

					dist_min     = min(dist_list)
			                index_dimer  = dist_list.index(dist_min)

#                                        index_dimer = int(len(dimer_list)*random.random())
                                        coord_new_exch     = np.delete(coord_new_exch, dimer_list[index_dimer][0], 0)
                                        coord_new_exch     = np.delete(coord_new_exch, dimer_list[index_dimer][1]-1, 0)
					veloc_new_exch     = np.delete(veloc_new_exch, dimer_list[index_dimer][0], 0)
					veloc_new_exch     = np.delete(veloc_new_exch, dimer_list[index_dimer][1]-1, 0)
                        		coord              = np.copy(coord_new_exch)
		                        velocity           = np.copy(veloc_new_exch)

########################################################################################################################################################

#Add a particle
                else:
                        a_rand = random.random()
                        if(a_rand<0.5):

				x_new = config.Latticvector[0]*random.random()
                                y_new = config.Latticvector[1]*random.random() 
                                z_new = config.zlab*random.random() + config.zmin
                                coord_new_exch = np.append(coord_new_exch, x_new+y_new+z_new)
                                coord_new_exch = np.reshape(coord_new_exch, (-1,3))
				atag = is_acceptable(coord_new_exch)
               	                if (atag == True):
                       	                coord = np.copy(coord_new_exch)

                        else:
                                comx = config.Latticvector[0]*random.random() 
                                comy = config.Latticvector[1]*random.random()
				comz = config.zlab*random.random() + config.zmin
#                                comz = config.Latticvector[2]*random.random()
				H2_com = comx + comy + comz

                                theta=random.random()*config.pi
                                phi=random.random()*2.0*config.pi
                                x1_new = 0.5*config.dist[1][1]*math.sin(theta)*math.cos(phi) + H2_com[0]
                                y1_new = 0.5*config.dist[1][1]*math.sin(theta)*math.sin(phi) + H2_com[1]
                                z1_new = 0.5*config.dist[1][1]*math.cos(theta) + H2_com[2]
				

                                x2_new = 0.5*config.dist[1][1]*math.sin(config.pi-theta)*math.cos(config.pi+phi) + H2_com[0]
                                y2_new = 0.5*config.dist[1][1]*math.sin(config.pi-theta)*math.sin(config.pi+phi) + H2_com[1]
                                z2_new = 0.5*config.dist[1][1]*math.cos(config.pi-theta) + H2_com[2]


                                dist_H2=math.sqrt((x1_new-x2_new)**2+(y1_new-y2_new)**2+(z1_new-z2_new)**2)


                                coord_new_exch = np.append(coord_new_exch, [x1_new,y1_new,z1_new])
                                coord_new_exch = np.reshape(coord_new_exch, (-1,3))
				atag = is_acceptable(coord_new_exch)

               	                if (atag == True):
                       	                coord_new_exch = np.append(coord_new_exch, [x2_new,y2_new,z2_new])
                               	        coord_new_exch = np.reshape(coord_new_exch, (-1,3))
					atag2 = is_acceptable(coord_new_exch)
                        	        if (atag2 == True):
                                		coord = np.copy(coord_new_exch)


        else:
                a_rand = random.random()
                if(a_rand<0.5):
        	        x_new = config.Latticvector[0]*random.random()
                        y_new = config.Latticvector[1]*random.random()
			z_new = config.zlab*random.random() + config.zmin
 #                       z_new = config.Latticvector[2]*random.random()
                        coord_new_exch = np.append(coord_new_exch, x_new+y_new+z_new)
                        coord_new_exch = np.reshape(coord_new_exch, (-1,3))
			atag = is_acceptable(coord_new_exch)
                        if (atag == True):
                  	      coord = np.copy(coord_new_exch)

                else:
                	comx = config.Latticvector[0]*random.random()
                        comy = config.Latticvector[1]*random.random()
			comz = config.zlab*random.random() + config.zmin
#                        comz = config.Latticvector[2]*random.random()
                        H2_com = comx + comy + comz

                        theta=random.random()*config.pi
                        phi=random.random()*2.0*config.pi
                        x1_new = 0.5*config.dist[1][1]*math.sin(theta)*math.cos(phi) + H2_com[0]
                        y1_new = 0.5*config.dist[1][1]*math.sin(theta)*math.sin(phi) + H2_com[1]
                        z1_new = 0.5*config.dist[1][1]*math.cos(theta) + H2_com[2]


                        x2_new = 0.5*config.dist[1][1]*math.sin(config.pi-theta)*math.cos(config.pi+phi) + H2_com[0]
                        y2_new = 0.5*config.dist[1][1]*math.sin(config.pi-theta)*math.sin(config.pi+phi) + H2_com[1]
                        z2_new = 0.5*config.dist[1][1]*math.cos(config.pi-theta) + H2_com[2]


                        dist_H2=math.sqrt((x1_new-x2_new)**2+(y1_new-y2_new)**2+(z1_new-z2_new)**2)


                        coord_new_exch = np.append(coord_new_exch, [x1_new,y1_new,z1_new])
                        coord_new_exch = np.reshape(coord_new_exch, (-1,3))
			atag = is_acceptable(coord_new_exch)

                        if (atag == True):
	                	coord_new_exch = np.append(coord_new_exch, [x2_new,y2_new,z2_new])
                                coord_new_exch = np.reshape(coord_new_exch, (-1,3))
				atag2 = is_acceptable(coord_new_exch)
                                if (atag2 == True):
                                        coord = np.copy(coord_new_exch)
	return coord, velocity

def accept_r(T, Mu, e_f, e_a, nummber):
        T  = float(T)
        beta = 1.0/(T*config.kB)
#########################################################################################
##       1 u/Da = 931MeV/(c)**2 = 931*1.0e6 eV/(c)**2                                  ##
##       lamada = math.sqrt(h**2/(2*pi*mass_O)*kb*T))*c*(1.0e3)/31.0                   ##
#########################################################################################
#       lamada = math.sqrt(h**2/(2*pi*mass[1])*beta)*c/(31.0*(1.0e3))
#########################################################################################
##    ---grand canonical coupled to an ideal gas bath with pressure: pid               ##
##        chemical potential bath: mu^b = mu^0 + ln(beta*pid)/beta                     ##
##                                      = ln (beta*pid*Lamda^3)/beta                   ##
##        zz is defined as         zz   = exp(beta*mu^b)/Lamda^3                       ##
##                                      = beta*pid                                     ##
##        excess chemical pot.     muex = mu^b -mu^id                                  ##
##                                      = mu^0 + ln(beta*pid)/beta - mu^0 - ln(rho)    ##
##                                      = ln(zz)/beta - ln <rho>                       ##
#########################################################################################      
#       ZZ = math.exp(beta*mu)/((lamada)**3)
#       ZZ = beta*pid
#       ZZ = cal_zz(Mu, T)
	num_adsorbate = nummber-config.n_metal
        lamd = cal_lamada(T)
	test_gr = open('g_r','a+')
        test_gr.write('e_remove='+str(e_a)+' '*2+'e_former='+str(e_f)+' '*2+'e_diff='+str(e_a-e_f)+'\n')
        test_gr.write('e_collect='+str(e_a-e_f+config.EE)+'\n')
        test_gr.write('prefactor='+str(-beta*(e_a-e_f+config.EE+Mu))+'\n')

	if (-beta*(e_a-e_f+config.EE+Mu)>709):
		pacc_r = 1.0
	else:
	        pacc_r = (num_adsorbate*lamd**3)/(config.Vol)*(math.exp(-beta*(e_a-e_f+config.EE+Mu)))
	test_gr.write('P_r='+str(pacc_r)+'\n')
	test_gr.close()
	return pacc_r

def accept_r2(T, Mu, e_f, e_a, nummber):
        T  = float(T)
        beta = 1.0/(T*config.kB)
#########################################################################################
##       1 u/Da = 931MeV/(c)**2 = 931*1.0e6 eV/(c)**2                                  ##
##       lamada = math.sqrt(h**2/(2*pi*mass_O)*kb*T))*c*(1.0e3)/31.0                   ##
#########################################################################################
#       lamada = math.sqrt(h**2/(2*pi*mass[1])*beta)*c/(31.0*(1.0e3))
#########################################################################################
##    ---grand canonical coupled to an ideal gas bath with pressure: pid               ##
##        chemical potential bath: mu^b = mu^0 + ln(beta*pid)/beta                     ##
##                                      = ln (beta*pid*Lamda^3)/beta                   ##
##        zz is defined as         zz   = exp(beta*mu^b)/Lamda^3                       ##
##                                      = beta*pid                                     ##
##        excess chemical pot.     muex = mu^b -mu^id                                  ##
##                                      = mu^0 + ln(beta*pid)/beta - mu^0 - ln(rho)    ##
##                                      = ln(zz)/beta - ln <rho>                       ##
#########################################################################################      
#       ZZ = math.exp(beta*mu)/((lamada)**3)
#       ZZ = beta*pid
#       ZZ = cal_zz(Mu, T)
        num_adsorbate = nummber-config.n_metal
        lamd = cal_lamada(T)
        test_gr = open('g_r2','a+')
        test_gr.write('e_remove='+str(e_a)+' '*2+'e_former='+str(e_f)+' '*2+'e_diff='+str(e_a-e_f)+'\n')
        test_gr.write('e_collect='+str(e_a-e_f+2*config.EE)+'\n')
        test_gr.write('prefactor='+str(-beta*(e_a-e_f+2*config.EE+2*Mu))+'\n')

        if (-beta*(e_a-e_f+2*config.EE+2*Mu)>709):
                pacc_r = 1.0
        else:
                pacc_r = (num_adsorbate*lamd**3)/(config.Vol)*(math.exp(-beta*(e_a-e_f+2*config.EE+2*Mu)))
        test_gr.write('P_r='+str(pacc_r)+'\n')
	test_gr.close()
        return pacc_r

def accept_a(T, Mu, e_f, e_a, nummber):
        T  = float(T)
        beta = 1.0/(T*config.kB)
#########################################################################################
##       1 u/Da = 931MeV/(c)**2 = 931*1.0e6 eV/(c)**2                                  ##
##       lamada = math.sqrt(h**2/(2*pi*mass_O)*kb*T))*c*(1.0e3)/31.0                   ##
#########################################################################################
#       lamada = math.sqrt(h**2/(2*pi*mass[1])*beta)*c/(31.0*(1.0e3))
#########################################################################################
##    ---grand canonical coupled to an ideal gas bath with pressure: pid               ##
##        chemical potential bath: mu^b = mu^0 + ln(beta*pid)/beta                     ##
##                                      = ln (beta*pid*Lamda^3)/beta                   ##
##        zz is defined as         zz   = exp(beta*mu^b)/Lamda^3                       ##
##                                      = beta*pid                                     ##
##        excess chemical pot.     muex = mu^b -mu^id                                  ##
##                                      = mu^0 + ln(beta*pid)/beta - mu^0 - ln(rho)    ##
##                                      = ln(zz)/beta - ln <rho>                       ##
#########################################################################################      
#       ZZ = math.exp(beta*mu)/((lamada)**3)
#       ZZ = beta*pid
#       ZZ = cal_zz(Mu, T)
        lamd = cal_lamada(T)
	num_adsorbate = nummber-config.n_metal
	test_ga=open('g_a','a+')
	test_ga.write('e_add='+str(e_a)+' '*2+'e_former='+str(e_f)+'e_diff='+str(e_a-e_f)+'\n')
        test_ga.write('e_collect='+str(e_a-e_f-config.EE)+'\n')
        test_ga.write('prefactor='+str(-beta*(e_a-e_f-config.EE-Mu))+'\n')
	if (-beta*(e_a-e_f-config.EE-Mu)>709):
		pacc_a = 1.0
	else:
	        pacc_a = config.Vol/((num_adsorbate + 1)*lamd**3)*(math.exp(-beta*(e_a-e_f-config.EE-Mu)))
	test_ga.write('p_a='+str(pacc_a)+'\n')
	test_ga.close()
	return pacc_a

def accept_a2(T, Mu, e_f, e_a, nummber):
        T  = float(T)
        beta = 1.0/(T*config.kB)
#########################################################################################
##       1 u/Da = 931MeV/(c)**2 = 931*1.0e6 eV/(c)**2                                  ##
##       lamada = math.sqrt(h**2/(2*pi*mass_O)*kb*T))*c*(1.0e3)/31.0                   ##
#########################################################################################
#       lamada = math.sqrt(h**2/(2*pi*mass[1])*beta)*c/(31.0*(1.0e3))
#########################################################################################
##    ---grand canonical coupled to an ideal gas bath with pressure: pid               ##
##        chemical potential bath: mu^b = mu^0 + ln(beta*pid)/beta                     ##
##                                      = ln (beta*pid*Lamda^3)/beta                   ##
##        zz is defined as         zz   = exp(beta*mu^b)/Lamda^3                       ##
##                                      = beta*pid                                     ##
##        excess chemical pot.     muex = mu^b -mu^id                                  ##
##                                      = mu^0 + ln(beta*pid)/beta - mu^0 - ln(rho)    ##
##                                      = ln(zz)/beta - ln <rho>                       ##
#########################################################################################      
#       ZZ = math.exp(beta*mu)/((lamada)**3)
#       ZZ = beta*pid
#       ZZ = cal_zz(Mu, T)
        lamd = cal_lamada(T)
        num_adsorbate = nummber-config.n_metal
        test_ga=open('g_a2','a+')
        test_ga.write('e_add='+str(e_a)+' '*2+'e_former='+str(e_f)+'e_diff='+str(e_a-e_f)+'\n')
        test_ga.write('e_collect='+str(e_a-e_f-2*config.EE)+'\n')
        test_ga.write('prefactor='+str(-beta*(e_a-e_f-2*config.EE-2*Mu))+'\n')
        if (-beta*(e_a-e_f-2*config.EE-2*Mu)>709):
                pacc_a = 1.0
        else:
                pacc_a = config.Vol/((num_adsorbate + 2)*lamd**3)*(math.exp(-beta*(e_a-e_f-2*config.EE-2*Mu)))
        test_ga.write('p_a='+str(pacc_a)+'\n')
        test_ga.close()
        return pacc_a
