import math
import config
import numpy as np


def is_acceptable(struct):
    # TODO: This MUST be made more efficent. a huge point of lag here
    number_origin = len(struct)
    superstructure = create2D_superstructure(struct) ###surface
    #print len(superstructure)
###see the supercell##
#    filestruct=open('196.xyz', 'a+')
 #   filestruct.write(str(196)+'\n'*2)
  #  for i in range(len(superstructure)):
#	index=i/number_origin
#	if i-index*number_origin<config.n_metal:
#		filestruct.write('%8s %15.5f %15.5f %15.5f \n' %('Si', superstructure[i][0], superstructure[i][1], superstructure[i][2]))
#	else:
#		filestruct.write('%8s %15.5f %15.5f %15.5f \n' %('H', superstructure[i][0], superstructure[i][1], superstructure[i][2]))
#superstructure = create3D_superstructure(struct)    ###bulk
    result = check_closeness(superstructure, config.closeness_ratio, number_origin)
    return result

###bulk material
def create3D_superstructure(original_struct):
    lv = np.array(config.Latticvector, dtype=float)    
    number_atom = len(original_struct)
    original_struct = np.array(original_struct)
    superstructure = []
    # construct image
    superstructure = np.append(superstructure, orginal_struct+[0.0, 0.0, 0.0,])
    superstructure = np.append(superstructure, orginal_struct+lv[0])
    superstructure = np.append(superstructure, orginal_struct+lv[1])
    superstructure = np.append(superstructure, orginal_struct+lv[2])
    superstructure = np.append(superstructure, orginal_struct+lv[0]+lv[1])
    superstructure = np.append(superstructure, orginal_struct+lv[1]+lv[2])
    superstructure = np.append(superstructure, orginal_struct+lv[2]+lv[0])
    superstructure = np.append(superstructure, orginal_struct+lv[0]+lv[1]+lv[2])
    superstructure = np.reshape(superstructure, (-1,3))
    return superstructure

##surface
def create2D_superstructure(original_struct):
    lv = np.array(config.Latticvector, dtype=float)
#    lv=[[11.489, 0.0, 0.0],[5.745, 9.95, 0.0],[0.0,0.0,48.0]]
    number_atom = len(original_struct)
    original_struct = np.array(original_struct)
    superstructure = []
    # construct image
    superstructure = np.append(superstructure, original_struct+[0.0, 0.0, 0.0,])
    superstructure = np.append(superstructure, original_struct+lv[0])
    superstructure = np.append(superstructure, original_struct+lv[1])
    superstructure = np.append(superstructure, original_struct+lv[0]+lv[1])
    superstructure = np.reshape(superstructure, (-1,3))
    return superstructure

    
def check_closeness(structure, closeness_ratio, number_atom):
    #print number_atom
    c_a = 0
    for atom_a in structure:
        c_b = 0
	tmp_index = c_a/number_atom
        if (c_a-tmp_index*number_atom)<config.n_metal:
	        rad_a = config.atom_radius[0]  #radii.get(atom_a['element'])
	else:
		rad_a = config.atom_radius[1]
        for atom_b in structure:
            if c_a == c_b: c_b += 1; continue
            c_b += 1
	    tmp_index = (c_b-1)/number_atom
	    if  (c_b-1-tmp_index*number_atom)<config.n_metal:
            	rad_b = config.atom_radius[0]  #radii.get(atom_a['element'])
            else:
                rad_b = config.atom_radius[1]
	
            dist = math.sqrt((atom_a[0] - atom_b[0]) ** 2 + 
                             (atom_a[1] - atom_b[1]) ** 2 + 
                             (atom_a[2] - atom_b[2]) ** 2)
            #print closeness_ratio, rad_a, rad_b
            threshold = closeness_ratio * (rad_a + rad_b)
	    #print dist, threshold
	    #print c_a, c_b
            if dist < threshold: return False
        c_a += 1
    return True
#struct=[[0.0, 0.0, 51.12693], [3.82969, 0.0, 51.12693], [7.65938, 0.0, 51.12693], [1.91485, 3.31661, 51.12693], [5.74454, 3.31661, 51.12693], [9.57423, 3.31661, 51.12693], [3.82969, 6.63322, 51.12693], [7.65938, 6.63322, 51.12693], [11.48907, 6.63322, 51.12693], [0.0, 2.21107, 48.0], [3.82969, 2.21107, 48.0], [7.65938, 2.21107, 48.0], [1.91485, 5.52768, 48.0], [5.74454, 5.52768, 48.0], [9.57423, 5.52768, 48.0], [3.82969, 8.84429, 48.0], [7.65938, 8.84429, 48.0], [11.48907, 8.84429, 48.0], [5.7003, 1.05718, 47.92007], [1.95086, 3.37926, 47.83984], [9.5051, 3.31717, 47.86663], [1.96499, 0.9299, 47.94449], [0.00136, 6.63581, 47.92568], [9.61984, 7.82177, 47.77189], [9.54425, 1.26258, 47.99384], [5.77453, 3.22683, 47.93851], [0.01383, 4.49257, 47.96442], [5.67226, 7.91251, 47.76206], [7.62772, 6.4363, 47.80791], [7.63481, 4.4346, 47.94039], [3.62348, 6.67957, 47.92626], [1.79461, 7.82246, 47.56697], [3.82378, 4.42591, 47.99601], [9.57272, 8.83512, 47.23236], [1.94293, 8.83542, 47.0651], [5.73824, 8.83929, 47.18993], [2.48998, 1.02042, 46.77665], [1.84467, 2.12515, 46.90125], [5.69775, 2.07222, 46.99335], [9.97561, 0.95727, 46.913], [6.12613, 0.95481, 46.77005], [0.60028, 7.0223, 47.10471], [9.56376, 2.16972, 47.06188], [0.00044, 5.81688, 47.0981], [10.65517, 7.09265, 47.19843], [4.80949, 7.29302, 46.96189], [5.03767, 3.75726, 46.93225], [0.59589, 4.01576, 47.14149], [1.25043, 1.08962, 46.64988]]
#print len(struct)
#lattice_vector=[[11.489, 0.0, 0.0],[5.745, 9.95, 0.0],[0.0,0.0,48.0]]
#tag=is_acceptable(struct)
#print tag
