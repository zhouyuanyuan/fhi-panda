import config
from tool import *
import numpy
import os
def write_data(number_step, arr_T, arr_u, arr_e, arr_H, list_sequence, list_coord, list_veloc, arr_Na, arr_Nr):
        step    = number_step
        T       = arr_T.flatten()
        u       = arr_u.flatten()
        e       = arr_e.flatten()
	pH      = arr_H.flatten()
        queue   = list_sequence
        coord   = list_coord
	veloc   = list_veloc
	for i_rank in range(len(T)):
	        for i in range(config.num_T):
        	        for j in range(config.num_u):
	                        if T[i_rank] == config.T[i] and u[i_rank] == config.Mu[j]:
        	                        nn = two2one(i,j)
					snn='%.2d' %nn
					caldir = 'replica_'+snn
					output3 = open(os.path.join(caldir, 'replica_' + str(float(T[i_rank])) + '_' + str(float(u[i_rank])) + '.dat'), 'a+')
					n1 = config.n_metal
					n2 = len(coord[i_rank])-n1
				        output3.write(' '*10 + 'Current_step= ' + str(step)+ '\n'
				                      + ' '+ '+'*34 + '\n'
				                      + ' temperature = ' + str(float(T[i_rank])) +'\n'
			                              + ' Mu = ' + str(float(u[i_rank])) + '\n'
			                     	      + ' Energy = ' +str(float(e[i_rank])) + '\n'
						      + ' pseudoH= ' +str(float(pH[i_rank]))+ '\n'
			                     	      + ' N_a = ' +str(int(arr_Na[i_rank])) + '\n'
			                     	      + ' N_r = ' +str(int(arr_Nr[i_rank])) + '\n'
			                     	      + ' Ele_Num = '+' '*5 + str(n1) +' '*5 + str(n2) + '\n'
			                     	      + '-'*34 + '\n'
			                     	      + '-'*34 + '\n')
#					for k in range(len(coord[i_rank])):
#						output3.write(' '*4+'%15.5f %15.5f %15.5f\n' %(coord[i_rank][k][0], coord[i_rank][k][1], coord[i_rank][k][2]))
#        				output3.close()
#		for ii in range(200):
#			if n2 == ii:
#                		output4 = open('structure_' + str(ii),'a+')
#				output4.write(' '*10 + 'Current_step= ' + str(step)+ '\n'
#                                              + ' '+ '+'*34 + '\n'
#                                              + ' temperature = ' + str(float(T[i_rank])) +'\n'
#                                              + ' Mu = ' + str(float(u[i_rank])) + '\n'
#                                              + ' Energy = ' +str(float(e[i_rank])) + '\n'
#					      + ' pseudoH= ' +str(float(pH[i_rank]))+ '\n'
#                                              + ' N_a = ' +str(int(arr_Na[i_rank])) + '\n'
#                                              + ' N_r = ' +str(int(arr_Nr[i_rank])) + '\n'
#                                              + ' Ele_Num = '+' '*5 + str(n1) +' '*5 + str(n2) + '\n'
#                                              + '-'*34 + '\n')
#				for k in range(len(coord[i_rank])):
#       		                        output4.write(' '*4+'%15.5f %15.5f %15.5f\n' %(coord[i_rank][k][0], coord[i_rank][k][1], coord[i_rank][k][2]))
#				output4.write('-'*34 + '\n'*2)
#		        	output4.close() 


# Write the restart file
        output5 = open('restart.dat','w')
        output5.write('Current_step= ' + str(step)+ '\n')
        output5.write('Temperature = ')
        for i in range(config.num_T*config.num_u):
                output5.write(str(T[i]) + ' '*2)
        output5.write('\n' + 'Mu = ')
        for i in range(config.num_T*config.num_u):
                output5.write(str(u[i]) + ' '*2)
        output5.write('\n' + 'Energy = ')
        for i in range(config.num_T*config.num_u):
                output5.write(str(float(e[i])) + ' '*2)
        output5.write('\n' + 'pseudoH = ')
        for i in range(config.num_T*config.num_u):
                output5.write(str(float(pH[i])) + ' '*2)
	output5.write('\n' + 'sequence = ')
        for i in range(config.num_T):
                for j in range(config.num_u):
                        output5.write(str(queue[i][j]) + ' '*2)
        output5.write('\n' + 'N_a = ')
        for i in range(config.num_T*config.num_u):
                output5.write(str(int(arr_Na[i])) + ' '*2)
        output5.write('\n' + 'N_r = ')
        for i in range(config.num_T*config.num_u):
                output5.write(str(int(arr_Nr[i])) + ' '*2)
        output5.write('\n' + 'Numberofparticle = ')
        for i in range(config.num_T*config.num_u):
                n0          = len(coord[i])
                output5.write(str(n0) + ' '*2)
        output5.write('\n' + '#Restart structures\n')
        for i in range(config.num_T*config.num_u):
                n0          = len(coord[i])
                n2          = len(coord[i])-n0
                output5.write('@replica' + str(i) + 'atomposition'+'\n')
                for k in range(len(coord[i])):
			output5.write(' '*4+'%15.5f %15.5f %15.5f\n' %(coord[i][k][0], coord[i][k][1], coord[i][k][2]))                        
                output5.write('@End\n')
	output5.write('\n' + '#Restart velocities\n')
        for i in range(config.num_T*config.num_u):
                output5.write('@replica' + str(i) + 'velocity'+'\n')
                for k in range(len(veloc[i])):
                        output5.write(' '*4+'%15.5f %15.5f %15.5f\n' %(veloc[i][k][0], veloc[i][k][1], veloc[i][k][2]))        
                output5.write('@End\n')
        output5.close()
