import random
import sys
import os
import time
import copy
import config
import math
from tool import *
import numpy as np

# The acceptance ratio of replica exchange. 
def Exchange_acceptance(temperature1, temperature2, E_potential1, E_potential2, mu1, mu2, N1, N2):
	delta_beta = (1.0/temperature1-1.0/temperature2)*(1.0/config.kB)
        delta_E = E_potential2-E_potential1-config.EE*(N2-N1)
        delta_betamu = (mu1/temperature1-mu2/temperature2)*(1.0/config.kB)
        delta_N = N2-N1
	judge_para = ((-delta_beta*delta_E) + delta_betamu*delta_N)
	suffix_gc  = ((temperature1/temperature2)**(3.0/2.0*delta_N))
#	if judge_para >1:
#		P_para = 1.0
#	else:
#		P_para = math.exp(judge_para)
	P_para = math.exp(judge_para)*suffix_gc
        P=min(1, P_para)
        if(P>random.random()):
        	tag=1
        else:
                tag=0

        return tag

#def Neighbouring_swap_replicas(arr_T, arr_u, arr_vs, arr_e, arr_N, mapp):
def Neighbouring_swap_replicas(arr_T, arr_u, arr_e, arr_N, mapp):
	rex_T        = np.copy(arr_T.flatten())
	rex_u        = np.copy(arr_u.flatten())
#	rex_v        = np.copy(arr_vs.flatten())
	rex_N        = np.copy(arr_N.flatten())
	rex_e        = np.copy(arr_e.flatten())
	rex_map      = np.copy(mapp)
	output1 = open('log_rex', 'a+')
        output1.write('#'*7 + 'Start REX '+'#'*17 + '\n')
        localtime = time.asctime(time.localtime(time.time()))
        output1.write(str(localtime)+'\n')
	rand = random.random()
	if(rand <= 0.25):
		swap_type = 0
	elif(0.25 < rand <= 0.50):
		swap_type = 1
	elif(0.50 < rand <= 0.75):
		swap_type = 2
	elif(0.75 < rand):
		swap_type = 3
#	output1.write('Tt' + ' '*10 )
	state_tot = []
#        for i in range(config.num_T):
#		for  j in range(config.num_u):
#			output1.write(' '*2 + str(rex_T[rex_map[i][j]]))
        output1.write('\n' + 'T_array' + ' '*10 + str(rex_T))
	output1.write('\n' + 'Mu_array' + ' '*10 + str(rex_u))
#	output1.write('\n' + 'Mu' + ' '*10)
#	for i in range(config.num_T):
#	        for j in range(config.num_u):
#	       	        output1.write(' '*2 +str(rex_u[rex_map[i][j]]) + ' '*2)
#	output1.write('\n' + 'Vscale_array' + ' '*10 + str(rex_v))
#	output1.write('\n' + 'Vscale' + ' '*10)
#	for i in range(config.num_T):
#                for j in range(config.num_u):
#                        output1.write(' '*2 +str(rex_v[rex_map[i][j]]) + ' '*2)
	output1.write('\n' + 'E' + ' '*10)
        for i in range(config.num_T):
                 for j in range(config.num_u):
                         output1.write(' '*2 +str(float(rex_e[rex_map[i][j]])))
#	output1.write('\n' + 'E_array' + ' '*10 + str(rex_e))
	output1.write('\n' + 'N' + ' '*10)
	for i in range(config.num_T):
                 for j in range(config.num_u):
                         output1.write(' '*2 +str(rex_N[rex_map[i][j]]))
#	output1.write('\n' + 'N_array' + ' '*10 + str(rex_N))
	output1.write('\n'+ 'map' + ' '*6+'\n')
        for i in range(config.num_T):
                for j in range(config.num_u):
                        output1.write('%2d' %(rex_map[i][j]) + ' '*3)
                output1.write('\n')
	output2=open('swap.info', 'r')
        parameters=output2.readlines()
        for parameter in parameters:
 		tmp = []
        	tmp=parameter.split()
                oneortwo=int(tmp[0])
        if(oneortwo==1):
               	start=0
                output1.write('Start from one because swap.info contains 1' + '\n')
                output2=open('swap.info', 'w')
                output2.write(str(2))
        elif(oneortwo==2):
                start=1
                output1.write('Start from two because swap.info contains 2' + '\n')
                output2=open('swap.info', 'w')
                output2.write(str(1))

###############################################################################
#    Mu1 Mu2  Mu3 Mu4 Mu5 Mu6 Mu7 Mu8 Mu9 Mu10                               #
# T1 ----    ----    ----    ----    ----                                    #
# T2  :       :       :       :       :                                      #
# T3  :       :       :       :       :                                      #
# T4   \       \       \      \      \                                       #
# T5    \       \       \      \      \                                      #
# T6   /       /       /       /     /                                       #
# T7  /       /       /        /    /                                        #
#                                                                            #
# swap_type = 0: ----  swap_type = 1: : swap_type = 2: \  swap_type = 3:/    #
#                                     :                \               /     #
###############################################################################
	if(swap_type == 0):
		output1.write('swap_type = 0 only swap temperature\n')
		for i in range(start, config.num_T-1, 2):
			for j in range(config.num_u):
				swap_group = []
				swap_group.append(str(rex_map[i][j]))
				swap_group.append(str(rex_map[i+1][j]))
				TT_1      = config.T[i]
				TT_2      = config.T[i+1]
				UU        = config.Mu[j]
				rand_acc = Exchange_acceptance(TT_1, TT_2,\
  							   rex_e[rex_map[i][j]], rex_e[rex_map[i+1][j]],\
							   UU, UU,\
							   rex_N[rex_map[i][j]], rex_N[rex_map[i+1][j]])
	                	if(rand_acc == 1):
					tmp                     = rex_map[i][j]
					rex_map[i][j]           = rex_map[i+1][j]
					rex_map[i+1][j]         = tmp
					rex_T[rex_map[i][j]]    = TT_1
					rex_T[rex_map[i+1][j]]  = TT_2
					#rex_v[rex_map[i][j]]	= math.sqrt(TT_1/TT_2)  #### before swapping T[3]=TT_1 with index(i,j), T[7]=TT_2 with index(i+1,j);after swapping, T[7]=TT_1 with index(i,j), T[3]=TT_2 with index(i+1,j). v[7]=math.sqrt(TT_1/TT_2), v[3]=1.0/v[7]
					#rex_v[rex_map[i+1][j]]   = 1.0/rex_v[rex_map[i][j]]
					output1.write('accepted  %s \n' %swap_group)
				 	btag = 1
				else:
					output1.write('rejected  %s \n'  %swap_group)	
       					btag = 0
		
	elif(swap_type == 1):
	        output1.write('swap_type = 1 only swap chemical potential\n')
		for i in range(config.num_T):
                        for j in range(start, config.num_u-1, 2):
                                swap_group = []
                                swap_group.append(str(rex_map[i][j]))
                                swap_group.append(str(rex_map[i][j+1]))
				TT        = config.T[i]
				UU_1      = config.Mu[j]
				UU_2      = config.Mu[j+1]
                                rand_acc = Exchange_acceptance(TT, TT, \
                                                           rex_e[rex_map[i][j]], rex_e[rex_map[i][j+1]], \
                                                           UU_1, UU_2,\
                                                           rex_N[rex_map[i][j]], rex_N[rex_map[i][j+1]]  )
                                if(rand_acc == 1):
					tmp                     = rex_map[i][j]
                                        rex_map[i][j]           = rex_map[i][j+1]
                                        rex_map[i][j+1]         = tmp
					rex_u[rex_map[i][j]]    = UU_1
					rex_u[rex_map[i][j+1]]  = UU_2
					output1.write('accepted  %s \n' %swap_group)
                                        btag = 1
                                else:
                                        output1.write('rejected  %s \n'  %swap_group)
                                        btag = 0

 
	elif(swap_type == 2):
       		output1.write('swap_type = 2 swap cross right\n')                
		for i in range(start, config.num_T-1,2):
                        for j in range(config.num_u-1):
                                swap_group = []
                                swap_group.append(str(rex_map[i][j]))
                                swap_group.append(str(rex_map[i+1][j+1]))
				TT_1=config.T[i]
				TT_2=config.T[i+1]
				UU_1=config.Mu[j]
				UU_2=config.Mu[j+1]
                                rand_acc = Exchange_acceptance(TT_1, TT_2, \
                                                               rex_e[rex_map[i][j]], rex_e[rex_map[i+1][j+1]], \
                                                               UU_1, UU_2, \
                                                               rex_N[rex_map[i][j]], rex_N[rex_map[i+1][j+1]])
                                if(rand_acc == 1):
	                                tmp                     = rex_map[i][j]
                                        rex_map[i][j]           = rex_map[i+1][j+1]
                                        rex_map[i+1][j+1]       = tmp
					rex_u[rex_map[i][j]]    = UU_1
					rex_u[rex_map[i+1][j+1]]= UU_2
					rex_T[rex_map[i][j]]    = TT_1
					rex_T[rex_map[i+1][j+1]]= TT_2 
					#rex_v[rex_map[i][j]]   = math.sqrt(TT_1/TT_2)  #### before swapping T[3]=TT_1 with index(i,j), T[7]=TT_2 with index(i+1,j+1);after swapping, T[7]=TT_1 with index(i,j), T[3]=TT_2 with index(i+1,j+1). v[7]=math.sqrt(TT_1/TT_2), v[3]=1.0/v[7]
                                        #rex_v[rex_map[i+1][j+1]]   = 1.0/rex_v[rex_map[i][j]]
 
					output1.write('accepted  %s \n' %swap_group)
                                        btag = 1
                                else:
                                        output1.write('rejected  %s \n'  %swap_group)
                                        btag = 0

			
	elif(swap_type == 3):
	        output1.write('swap_type = 3 only swap cross left\n')
		for i in range(start, config.num_T-1, 2):
                        for j in range(1, config.num_u, 1):
                                swap_group = []
                                swap_group.append(str(rex_map[i][j]))
                                swap_group.append(str(rex_map[i+1][j-1]))
				TT_1     = config.T[i]
				TT_2     = config.T[i+1]
				UU_1     = config.Mu[j]
				UU_2     = config.Mu[j-1]
                                rand_acc = Exchange_acceptance(TT_1, TT_2,rex_e[rex_map[i][j]],\
							       rex_e[rex_map[i+1][j-1]], UU_1, UU_2,\
                                                               rex_N[rex_map[i][j]], rex_N[rex_map[i+1][j-1]])
				if(rand_acc == 1):
        	                        tmp                     = rex_map[i][j]
                                        rex_map[i][j]           = rex_map[i+1][j-1]
                                        rex_map[i+1][j-1]       = tmp
					rex_u[rex_map[i][j]]    = UU_1
					rex_u[rex_map[i+1][j-1]]= UU_2
					rex_T[rex_map[i][j]]    = TT_1
					rex_T[rex_map[i+1][j-1]]= TT_2
					#rex_v[rex_map[i][j]]   = math.sqrt(TT_1/TT_2)  #### before swapping T[3]=TT_1 with index(i,j), T[7]=TT_2 with index(i+1,j);after swapping, T[7]=TT_1 with index(i,j), T[3]=TT_2 with index(i+1,j). v[7]=math.sqrt(TT_1/TT_2), v[3]=1.0/v[7]
                                        #rex_v[rex_map[i+1][j-1]]   = 1.0/rex_v[rex_map[i][j]]
					output1.write('accepted  %s \n' %swap_group)
                                        btag = 1
                                else:
                                        output1.write('rejected  %s \n'  %swap_group)
                                        btag = 0
		
#	output1.write('Tt' + ' '*10)
#	for i in range(config.num_T):
#		for j in range(config.num_u):
#		        output1.write(' '*2 + str(rex_T[rex_map[i][j]]))
        output1.write('\n' + 'T_array' + ' '*10 + str(rex_T))
#	output1.write( '\n' + 'Mu' + ' '*10)
#	for i in range(config.num_T):
#		for j in range(config.num_u):
#	                output1.write(' '*2 + str(float(rex_u[rex_map[i][j]]))+ ' '*2)
	output1.write('\n' + 'Mu_array' + ' '*10 + str(rex_u))
#	output1.write('\n' + 'Vscale_array' + ' '*10 + str(rex_v))
#        output1.write('\n' + 'Vscale' + ' '*10)
#        for i in range(config.num_T):
#                for j in range(config.num_u):
#                        output1.write(' '*2 +str(float(rex_v[rex_map[i][j]])) + ' '*2)

	output1.write('\n' + 'E' + ' '*10)
	for i in range(config.num_T):
                 for j in range(config.num_u):
                         output1.write(' '*2 +str(float(rex_e[rex_map[i][j]])))
#       output1.write('\n' + 'E_array' + ' '*10 + str(rex_e))
	output1.write('\n' + 'N' + ' '*10)
        for i in range(config.num_T):
                 for j in range(config.num_u):
                         output1.write(' '*2 +str(int(rex_N[rex_map[i][j]])))
#        output1.write('\n' + 'N_array' + ' '*10 + str(rex_N))

	output1.write('\n'+ 'map' + ' '*6+'\n')
	for i in range(config.num_T):
		for j in range(config.num_u):
			output1.write('%2d' %(rex_map[i][j]) + ' '*3)
	        output1.write('\n')
	output1.write( '\n' + 'sequence' + ' '*6)
        for i in range(config.num_T):
		for j in range(config.num_u):
		        output1.write(str(int(rex_map[i][j]))+ ' '*6)
        output1.write('\n' + '####### End of rex step #################' +'\n'*2)
        output1.close()
#	return rex_T, rex_u, rex_v, rex_map 
	return rex_T, rex_u, rex_map
