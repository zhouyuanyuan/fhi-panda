#!/usr/bin/python

"""
Estimate -beta*E+mu*N*beta(Gibbs free energy), mu*N*beta, -beta*E(free energy) of regcmc data using MBAR.

PROTOCOL

* Potential energies and number of particle from regcmc simulation are read in by temperature and chemical potential
* Replica trajectories of potential energies and number of particle are reconstructed to reflect their true temporal
  correlation, and then subsampled to produce statistically independent samples, collecting them again by temperature
* The `pymbar` class is initialized to compute the dimensionless free energies/Gibbs free energy at each temperature(chemical potential) using MBAR
* The torsions are binned into sequentially labeled bins in two dimensions
* The relative free energies and uncertainties of these torsion bins at the temperature of interest is estimated
* The 2D PMF is written out

REFERENCES

[1] Shirts MR and Chodera JD. Statistically optimal analysis of samples from multiple equilibrium states.
J. Chem. Phys. 129:124105, 2008
http://dx.doi.org/10.1063/1.2978177
"""
import numpy
from math import *
import pymbar # for MBAR analysis
from pymbar import timeseries # for timeseries analysis
import commands
import os
import os.path
import timeit
import config
def read_file(filename):
	"""Read contents of the specified file.

	Parameters:
	-----------
	filename : str
	   The name of the file to be read

	Returns:
	lines : list of str
	   The contents of the file, split by line

	"""

	infile = open(filename, 'r')
	lines = infile.readlines()
	infile.close()

	return lines

def weight_regcmc(T_target, Mu_target):
#===================================================================================================
# IMPORTS
#===================================================================================================

	t_start= timeit.default_timer()
        
        #===================================================================================================
        # CONSTANTS
        #===================================================================================================
        
#        kB = 8.6173325e-5    # Boltzmann constant in eV/K
	kB = 1.3806503 * 6.0221415 / 4184.0 # Boltzmann constant in kcal/mol/K
	eV2kcal = 23.06035 
        
        #===================================================================================================
        # PARAMETERS
        #===================================================================================================
        data_directory = '/eos/scratch/yzhou/analysis-result/mbar/Si_Jan2_2018/Si4/data/' # directory containing the parallel tempering data
	print data_directory
        temperature_list_filename = os.path.join(data_directory, 'temperatures') # file containing temperatures in K
	chempot_list_filename     = os.path.join(data_directory, 'chemicalpotentials') # file containing chemical potentials in eV
        free_energies_filename    = open('f_k.out','a+')
	free_energies_filename.write('T_target='+str(T_target)+' '*6 + 'Mu_target='+str(Mu_target)+'\n')
        potential_energies_filename = os.path.join(data_directory, 'energies', 'potential-energies') # file containing total energies (in eV) for each (temperature, chemical potential)and snapshot
	num_particle_filename     = os.path.join(data_directory, 'particles', 'num_particles')
	total_step		  = 625
#        trajectory_segment_length = 20 # number of snapshots in each contiguous trajectory segment
#        niterations = 500 # number of iterations to use
        target_temperature = T_target # target temperature for 2D PMF (in K)
	target_chempot     = (Mu_target)*eV2kcal # target chemical potential (in eV)
        nbins_per_torsion = 32 # number of bins per torsion dimension
        
        #===================================================================================================
        # MAIN
        #===================================================================================================
        
        #===================================================================================================
        # Read temperatures
        #===================================================================================================
        
        # Read list of temperatures.
        lines = read_file(temperature_list_filename)
        # Construct list of temperatures
        temperatures = lines[0].split()
        # Create numpy array of temperatures
        K = len(temperatures)
        temperature_k = numpy.zeros([K], numpy.float32) # temperature_k[k] is temperature of temperature index k in K
        for k in range(K):
           temperature_k[k] = float(temperatures[k])
        # Compute inverse temperatures
        beta_k = (kB * temperature_k)**(-1)
        # Read list of chemical potentials.
	lines = read_file(chempot_list_filename)
	# Construct list of chemical potentials
	chempots = lines[0].split()
	chempot_k = numpy.zeros([K], numpy.float32)
	for k in range(K):
		chempot_k[k] = float(chempots[k])*eV2kcal

        # Define other constants
        T = total_step#trajectory_segment_length * niterations # total number of snapshots per temperature
        
        #===================================================================================================
        # Read number of particles
        #===================================================================================================

        free_energies_filename.write("Reading number of particles...\n")
        Num_kt = numpy.zeros([K,T], numpy.float32) # U_kn[k,t] is the number of particle (dimensionless) for snapshot t of temperature index k
        lines = read_file(num_particle_filename)
        free_energies_filename.write("%d lines read, processing %d snapshots\n" % (len(lines), T))
        for t in range(T):
           # Get line containing the N for snapshot t of trajectory segment n
           line = lines[t]
           # Extract number values from text
           elements = line.split()
           for k in range(K):
              Num_kt[k,t] = float(elements[k])-config.n_metal
 

        #===================================================================================================
        # Read potential eneriges
        #===================================================================================================
        
        free_energies_filename.write("Reading potential energies...\n")
        U_kt = numpy.zeros([K,T], numpy.float32) # U_kn[k,t] is the potential energy (in eV) for snapshot t of temperature index k
        lines = read_file(potential_energies_filename)
        free_energies_filename.write("%d lines read, processing %d snapshots\n" % (len(lines), T))
        for t in range(T):
           # Get line containing the energies for snapshot t of trajectory segment n
           line = lines[t]
           # Extract energy values from text
           elements = line.split()
           for k in range(K):
              U_kt[k,t] = (float(elements[k])-Num_kt[k,t]*config.EE-config.n_metal*config.Emetal)*eV2kcal

 
	free_energies_filename.write(str(Num_kt)+'\n')
	free_energies_filename.write(str(U_kt)+'\n')
        #===================================================================================================
        #===================================================================================================
	# Read phi, psi trajectories
	#===================================================================================================

	free_energies_filename.write("Reading phi trajectories...\n")
	phi_kt = numpy.zeros([K,T], numpy.float32) # phi_kt[k,n,t] is phi angle (in degrees) for snapshot t of temperature k
	for k in range(K):
	   phi_filename = os.path.join(data_directory, 'trajectory', '%d.phi' % (k))	
	   phi_lines = read_file(phi_filename)
	   free_energies_filename.write("k = %d, %d phi lines read\n" % (k, len(phi_lines)))
	   for t in range(T):
	      # Extract phi and psi
	      phi_kt[k,t] = float(phi_lines[t])-config.n_metal

	# Read replica indices
        #===================================================================================================
        
        free_energies_filename.write("Reading replica indices...\n")
        filename = os.path.join(data_directory, 'replica-indices')
        lines = read_file(filename)
        replica_ik = numpy.zeros([T,K], numpy.int32) # replica_ki[i,k] is the replica index of temperature k for iteration i
        for i in range(T):
           elements = lines[i].split()
           for k in range(K):
              replica_ik[i,k] = int(elements[k])
        free_energies_filename.write("Replica indices for %d iterations processed.\n" % T)
        
        #===================================================================================================
        # Permute data by replica and subsample to generate an uncorrelated subset of data by temperature
        #===================================================================================================
        
        assume_uncorrelated = True
        if (assume_uncorrelated):
           # DEBUG - use all data, assuming it is uncorrelated
           free_energies_filename.write("Using all data, assuming it is uncorrelated...\n")
           U_kn = U_kt.copy()
	   phi_kn = phi_kt.copy()
	   Num_kn = Num_kt.copy()
           N_k = numpy.zeros([K], numpy.int32)
           N_k[:] = T
           N_max = T
###########################################################################################################################
	# assume the data is uncorrelated with each other
        else:
		pass
###########################################################################################################################        
        #===================================================================================================
        # Generate a list of indices of all configurations in kn-indexing
        #===================================================================================================
        
        # Create a list of indices of all configurations in kn-indexing.
        mask_kn = numpy.zeros([K,N_max], dtype=numpy.bool)
        for k in range(0,K):
           mask_kn[k,0:N_k[k]] = True
        # Create a list from this mask.
        indices = numpy.where(mask_kn)
        
        #===================================================================================================
        # Compute reduced potential energy of all snapshots at all temperatures
        #===================================================================================================


	free_energies_filename.write("Computing reduced gibbs free energies...\n")
        g_kln = numpy.zeros([K,K,N_max], numpy.float32) # u_kln[k,l,n] is reduced potential energy of trajectory segment n of temperature k evaluated at temperature l
        for k in range(K):
           for l in range(K):
              g_kln[k,l,0:N_k[k]] = beta_k[l] * U_kn[k,0:N_k[k]] - beta_k[l]*Num_kn[k,0:N_k[k]]*chempot_k[l] 
        #===================================================================================================
        # Bin torsions into histogram bins for PMF calculation
        #===================================================================================================
        
        # Here, we bin the (phi,psi) samples into bins in a 2D histogram.
        # We assign indices 0...(nbins-1) to the bins, even though the histograms are in two dimensions.
        # All bins must have at least one sample in them.
        # This strategy scales to an arbitrary number of dimensions.
        
        free_energies_filename.write("Binning torsions...\n")
        # Determine torsion bin size (in degrees)
        torsion_min = 0.0
        torsion_max = +16.0
        dx = (torsion_max - torsion_min) / float(nbins_per_torsion)
        # Assign torsion bins
        bin_kn = numpy.zeros([K,N_max], numpy.int16) # bin_kn[k,n] is the index of which histogram bin sample n from temperature index k belongs to
        nbins = 0
        bin_counts = list()
        bin_centers = list() # bin_centers[i] is a (phi,psi) tuple that gives the center of bin i
        for i in range(nbins_per_torsion):
              # Determine (phi) of bin center.
           phi = torsion_min + dx * (i + 0.5)
        
           # Determine which configurations lie in this bin.
           in_bin = (phi-dx/2 <= phi_kn[indices]) & (phi_kn[indices] < phi+dx/2)
        
              # Count number of configurations in this bin.
           bin_count = in_bin.sum()
        
              # Generate list of indices in bin.
           indices_in_bin = (indices[0][in_bin], indices[1][in_bin])
        
           if (bin_count > 0):
                 # store bin (phi,psi)
           	bin_centers.append( (phi) )
                bin_counts.append( bin_count )
        
                 # assign these conformations to the bin index
                bin_kn[indices_in_bin] = nbins
        
                 # increment number of bins
                nbins += 1
        
        free_energies_filename.write("%d bins were populated:\n" % nbins)
        for i in range(nbins):
           free_energies_filename.write("bin %5d %6.1f %12d conformations\n" % (i, bin_centers[i], bin_counts[i]))
        
        #===================================================================================================
        # Initialize MBAR.
        #===================================================================================================
        
        mbar = pymbar.MBAR(g_kln, N_k, verbose=True)
	#===================================================================================================
        # Compute PMF at the desired temperature.
        #===================================================================================================

        free_energies_filename.write("Computing potential of mean force...\n")

        # Compute reduced potential energies at the temperaure of interest
        target_beta = 1.0 / (kB * target_temperature)
        g_kn = target_beta * U_kn - target_beta*Num_kn*target_chempot
        # Compute PMF at this temperature, returning dimensionless free energies and uncertainties.
        # f_i[i] is the dimensionless free energy of bin i (in kT) at the temperature of interest
        # df_i[i,j] is an estimate of the covariance in the estimate of (f_i[i] - f_j[j], with reference
        # the lowest free energy state.
        (g_i, dg_i) = mbar.computePMF(g_kn, bin_kn, nbins, uncertainties='from-lowest')

        # Show free energy and uncertainty of each occupied bin relative to lowest free energy
        free_energies_filename.write("2D PMF\n")
        free_energies_filename.write("\n")
        free_energies_filename.write(" %6s %6s %8s %10s %10s\n" % ('bin', 'phi', 'N', 'g', 'dg'))

        for i in range(nbins):
           free_energies_filename.write('%8d %6.1f %8d %10.3f %10.3f\n' % (i, bin_centers[i], bin_counts[i], g_i[i], dg_i[i]))
        GG_lowest = min(g_i)
	g_i = g_i.tolist()
        index_lowest = g_i.index(GG_lowest)
        GG_dic = {'bin':index_lowest, 'phi':bin_centers[index_lowest], 'N':bin_counts[index_lowest], 'g':g_i[index_lowest], 'dg':dg_i[index_lowest]}
	output1 = open('PTD','a+')
	output1.write("%8s %6s %8s %10s %10s\n" % ('bin', 'phi', 'N', 'g', 'dg'))
	output1.write('%8d %6.1f  %8d %10.3f %10.3f\n' % (GG_dic['bin'], GG_dic['phi'], GG_dic['N'], GG_dic['g'],GG_dic['dg']))
	t_end=timeit.default_timer()
	t_tot=t_end-t_start
	free_energies_filename.write('%8.3f s' %t_tot)
	free_energies_filename.close()
