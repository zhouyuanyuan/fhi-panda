# input the required data
import os
import numpy as np

workingdir = os.getcwd()
print(workingdir)
############### Constant ###############################
h      = 4.135667662e-15 # Plank constant unit is eV*s
kB     = 8.6173325e-5    # Boltzmann constant in eV/K
pi     = 3.14159265359   # the value of pi
c      = 2.99792458e18   # Speed of light in Angstrom/s
############## End of Constant ##########################
EE         = -15.8701367945  # half of binding energy of D2 molecule
#EE        = -13.598029095 # the energy of D particle
#EE       = -12.511280636 # the energy of the adsorbate particle
#EE       = -2040.874656679 # the energy of the adsorbate particle

if os.path.exists(str(workingdir)+'/REGC.dat'):
        finput = 'REGC.dat'
else:
        print('Error: no input file: REGC.dat')
        exit(0)

input1 = open(os.path.join(workingdir, finput), 'r')
indata = {}
data   = []
for line in input1:
    data.append(line)
    if '=' in line:
            if line.strip()[0] != '#':
                litem = line.split('=')
                indata[litem[0].strip().lower()] = litem[1].strip()
input1.close()
try:
    systemname = indata['systemname']
except:
        systemname = 'REGCMD'

try:
        numberofspecies = int(indata['numberofspecies'][0])
except:
    print('ERROR: please input number of species.')
    exit(0)

try:
    nameofatoms = []
    nameofatoms = indata['nameofatoms'].split()
except:
    print('ERROR: please input number of species.')
    exit(0)
try:
    tmp  = []
    tmp  = indata['radius'].split()
    radius  = float(tmp[0])
except:
    print('ERROR: please input radius.')
    exit(0)

Vol= 4.0*pi/3.0*radius**3

try:
        center = []
        center = indata['center'].split()
        center = [float(i) for i in center]
except:
        print('ERROR: please input center of spherical wall.')
        exit(0)

try:
        tmp = []
        tmp = indata['numberofatom'].split()
        n_atom = int(tmp[0])
except:
        print('ERROR: please input number of metal atom.')
        exit(0)


try:
    tmp = []
    tmp = indata['numberofmetalatom'].split()
    n_metal = int(tmp[0])
except:
        print('ERROR: please input number of metal atom.')
        exit(0)

try:
        dist = []
        for i in range(len(data)):
            if '@DistanceOfIon' in data[i]:
                for j in range(i+1, i+1+numberofspecies):
                    item = data[j].split()
                    tmp = []    
                    for k in range(numberofspecies):    
                        tmp.append(float(item[k]))
                    dist.append(tmp)
except:
    print('ERROR: please input distance of different atoms.')
    exit(0)

try:
    N_tmp = []
    N_tmp = indata['n_total'].split()
    N     = int(N_tmp[0])
except:
        print('ERROR: please input total number of loops.')
        exit(0)

try:
        mass = []
        mass = indata['mass'].split()
        mass = [float(i) for i in mass]
except:
    print('ERROR: please input atomic mass sequence.')
    exit(0)
try:
        tmp   = []
        tmp   = indata['num_t'].split()
        num_T = int(tmp[0])
except:
        print('ERROR: please input num_T.')
        exit(0)

try:
        tmp   = []
        tmp   = indata['num_u'].split()
        num_u = int(tmp[0])
except:
        print('ERROR: please input num_u.')
        exit(0)

try:
        T = []
        T = indata['temperature'].split()
        T = [float(i) for i in T]
except:
        print('ERROR: please input temperature sequence.')
        exit(0)

try:
        Mu = []
        Mu = indata['mu'].split()
        Mu = [float(i) for i in Mu]
except:
        print('ERROR: please input pressure sequence.')
        exit(0)

try:
        tmp = []
        tmp = indata['mdt'].split()
        mdt  = float(tmp[0])
except:
        print('ERROR: please input md time interval.')
        exit(0)


try:
        pickup = indata['pickup'][0]
        if pickup.upper() == 'T':
            pickup = True
        else:
            pickup = False
except:
        print('ERROR: please input restart or not.')
        exit(0)

if(pickup == False):
        try:
                tmp          = []
                tmp          = indata['current_step'].split()
                current_step = int(tmp[0])
        except:
                print('ERROR: please input current step.')
                exit(0)

        try:
                sequence = []
                for i in range(num_T):
                        for j in range(num_u):
                                sequence.append(num_u*i+j)
        except:
                print('ERROR: plese input initial sequence')


        Nacc_d = [0]*(num_T*num_u)
        Nacc_r = [0]*(num_T*num_u)
        Nacc_a = [0]*(num_T*num_u)
        Ham_list = [0.0]*(num_T*num_u)
        number_particle = [n_metal]*(num_T*num_u)
        try:
                e_list = [0.0]*(num_T*num_u)
        except:
                print('ERROR: please input energy sequence.')
                exit(0)

        try:
                atom_coord = []
                atom_veloc = []
                for i in range(len(data)):
                        if '@Atomposition' in data[i]:
                                for j in range(i+1, i+1+n_atom):
                                        item = data[j].split()
                                        tmp = []
                                        vmp = []
                                        for k in range(3):
                                                tmp.append(float(item[k]))
                                                vmp.append(0.0)
                                        atom_coord.append(tmp)
                                        atom_veloc.append(vmp)
        except:
                print('ERROR: please input initial structure of cluster.')
                exit(0)
        coord_list = []
        veloc_list = []
        for i in range(num_T):
                for i in range(num_u):
                        coord_list.append(atom_coord)
                        veloc_list.append(atom_veloc)
if(pickup == True):
        if os.path.exists(str(workingdir)+'/restart.dat'):
                finput = 'restart.dat'
        else:
                print('Error: no restart file: restart.dat')
                exit(0)

        input2 = open(os.path.join(workingdir, finput), 'r')
        indata = {}
        data   = []
        for line in input2:
                data.append(line)
                if '=' in line:
                    if line.strip()[0] != '#':
                        litem = line.split('=')
                        indata[litem[0].strip().lower()] = litem[1].strip()
        input2.close()


        try:
                tmp          = []
                tmp          = indata['current_step'].split()
                current_step = int(tmp[0]) + 1
        except:
                print('ERROR: please input current step.')
                exit(0)

        try:
                number_particle = []
                number_particle = indata['numberofparticle'].split()
                number_particle = [int(i) for i in number_particle]
        except:
                print('ERROR: please input temperature sequence.')
                exit(0)

        try:
                Nacc_r = []
                Nacc_r = indata['n_r'].split()
                Nacc_r = [int(i) for i in Nacc_r]
        except:
                print('ERROR: please input the number of acceptance_r')
                exit(0)

        try:
                Nacc_a = []
                Nacc_a = indata['n_a'].split()
                Nacc_a = [int(i) for i in Nacc_a]
        except:
                print('ERROR: please input the number of acceptance_a')
                exit(0)

        try:
                Ham_list = []
                Ham_list = indata['pseudoh'].split()
                Ham_list = [float(i) for i in Ham_list]
        except:
                print('ERROR: please input the number of pseduo-hamiltonian')
                exit(0)

        try:
                sequence = []
                sequence = indata['sequence'].split()
                sequence = [int(i) for i in sequence]
        except:
                print('ERROR: please input replica index')

        try:
                e_list = []
                e_list = indata['energy'].split()
                e_list = [float(i) for i in e_list]
        except:
                print('ERROR: please input energy sequence.')
                exit(0)

        try:
                coord_list = []
                for i in range(len(data)):
                        for j in range(num_T*num_u):
                                tmp_a = []
                                if ('@replica' + str(j) + 'atomposition') in data[i]:
                                        for k in range(i+1, i+1+number_particle[j]):
                                                tmp = data[k].split()
                                                tmp_a.append(map(float,tmp))
                                        coord_list.append(tmp_a)
        except:
                print('ERROR: please input restart structure of replicas.')
                exit(0)

        try:
                veloc_list = []
                for i in range(len(data)):
                        for j in range(num_T*num_u):
                                tmp_a = []
                                if ('@replica' + str(j) + 'velocity') in data[i]:
                                        for k in range(i+1, i+1+number_particle[j]):
                                                tmp = data[k].split()
                                                tmp_a.append(map(float,tmp))
                                        veloc_list.append(tmp_a)
        except:
                print('ERROR: please input restart velocities of replicas.')
                exit(0)

num_replica = int(num_T*num_u)
sequence=np.array(sequence).reshape((num_T,num_u))
index_table = []
for i in range(num_T*num_u):
        tmp = []
        tmp.append(*np.where(sequence == i)[0])
        tmp.append(*np.where(sequence == i)[1])
        index_table.append(tmp)
