import config 
import math
import os
import os.path
import numpy as np
import glob
import random

bohr2angstrom=0.52917

def two2one(a,b):
    index = config.num_u*a + b
    return index
def coru(R, Rho):
    sigma3 = sigma*sigma*sigma  
    Rc3    = sigma3/(R*R*R)
    CORU = 2.0*pi*epsilon**4*(Rho*sigma3)*(Rc3*Rc3*Rc3/9.0-Rc3/3.0)
    return(CORU)


def corp(R, Rho):
    sigma3 = sigma*sigma*sigma
    Rc3    = sigma3/(R*R*R)
    CORP = 4.0*pi*epsilon**4*(Rho**2)*sigma3*(2.0*Rc3*Rc3*Rc3/9.0-Rc3/3.0)
    return CORP

def adjust(Attempt, nacc_d, step):
    if Attempt == 0 and attempt > Attempt:
        nacc   = nacc_d
        attempt = Attempt    
    else:
        frac = float(nacc_d-nacc)/float(Attempt-attempt)
        dr   = step
        step = step*abs(frac/0.5)
        if(step/dr > 1.5):
            step = dr*1.5
        if(step/dr<0.5):
            step = dr*0.5
        if(step > hbox/2.0):
            step = hbox/2.0
        nacc = nacc_d
        attempt = Attempt
    return step

def cal_zz(Mu, T):
        t3=open('zz','a+')
        beta    = 1.0/(T*config.kB)
        lamada  = math.sqrt(config.h**2/(2*config.pi*config.mass[1])*beta)*config.c/(31.0*(1.0e3))
        zz      = math.exp(beta*Mu)/(lamada**3)
        t3.write('ZZ='+str(zz)+'\n')
        return zz

def cal_lamada(T):
        t4=open('lamda','a+')
        beta    = 1.0/(T*config.kB)
        lamada  = math.sqrt(config.h**2/(2*config.pi*config.mass[1])*beta)*config.c/(31.0*(1.0e3))
        t4.write('lamda='+str(lamada)+'\n')
        return lamada

def write_geometry(structure, velocity, rank):
    srank='%.2d' %rank
    tmp_file=os.path.join('replica_'+srank+'/', 'geometry.in')
    if os.path.isfile(tmp_file):
        os.system('mv replica_'+str(srank)+'/geometry.in '+'replica_'+str(srank)+ '/last-geometry.in')
        geometry_file = open(os.path.join('replica_'+srank+'/', 'geometry.in'), 'w')
        for ii in range(len(structure)):
            if ii<config.n_metal:
                    geometry_file.write('%8s %15.5f %15.5f %15.5f %8s\n' %('atom', structure[ii][0], structure[ii][1], structure[ii][2], config.nameofatoms[0]))
            else:
                    geometry_file.write('%8s %15.5f %15.5f %15.5f %8s\n' %('atom', structure[ii][0], structure[ii][1], structure[ii][2], config.nameofatoms[1]))
    #   if ii<config.n_metal:
    #       geometry_file.write('%8s %15.5f %15.5f %15.5f\n' %('velocity', velocity[ii][0], velocity[ii][1], velocity[ii][2]))
    geometry_file.close()

def write_md_control(T_local, rank, tmp_number, i_loop):
    srank='%.2d' %rank
    md_control_file=open(os.path.join('replica_'+srank, 'control.in'), 'a+')
    md_control_file.write('MD_run    '+str(config.mdt)+' NVT_parrinello '+str(T_local)+' 0.02\n')
    md_control_file.write('MD_clean_rotations                 .true.\n')
    md_control_file.write('MD_MB_init                         '+str(T_local)+'\n')
    md_control_file.write('MD_time_step                       0.001\n')
    md_control_file.write('MD_restart                         .false.\n')
    md_control_file.write('plumed                             .true.\n')
    md_control_file.write('output_level MD_light\n')
    md_control_file.write(' MD_thermostat_units cm^-1\n')
    md_control_file.write('MD_maxsteps -1\n')
    md_control_file.write('postprocess_anyway .true.\n')

    plumed_file=open(os.path.join('replica_'+srank, 'plumed.dat'), 'w')
    plumed_file.write('HILLS HEIGHT 0.1 W_STRIDE 100\n')
    plumed_file.write('PRINT W_STRIDE 1\n')
    for iplum in range(tmp_number):
                plumed_file.write('DISTANCE LIST '+str(iplum+1)+ ' <g1> SIGMA 0.35 NOPBC\n')
    plumed_file.write('g1->\nLOOP 1 '+str(tmp_number)+' 1\ng1<-\n')
    plum_wall = config.radius/0.529177
    for iplum in range(tmp_number):
                plumed_file.write('UWALL CV '+str(iplum+1)+' LIMIT '+str(plum_wall)+' KAPPA 1.0\n')
    plumed_file.write('ENDMETA')
    plumed_file.close()

def read_convergence(filename):
        converged = False
        lines = open(filename, 'r').readlines()
        for n, line in enumerate(lines):
            if line.rfind('Have a nice day') > -1:
                converged = True
        return converged

def read_convergence2(filename):
        converged = False
        lines = open(filename, 'r').readlines()
        if lines[-2].rfind('Have a nice day') > -1:
                converged = True
        return converged


def read_energy(aimsout):
    for line in open(aimsout, 'r'):
        if line.rfind('Total energy corrected') > -1:
            E0 = float(line.split()[5])
        elif line.rfind('Total energy uncorrected') > -1:
            F = float(line.split()[5])
    return F, E0

def read_pseudoH(aimsout):
    for line in open(aimsout, 'r'):
        if line.rfind('BDP pseudo-Hamiltonian') > -1:
            pseudoH = float(line.split()[4])
    return pseudoH

def read_geometry(b_atom, aimsout):
    coord_MD=[]
    veloc_MD=[]
    coord   =[]
#        atom_veloc=[]
    tmp = []
    lines = open(aimsout, 'r').readlines()
    for n, line in enumerate(lines):
            if line.rfind('Final atomic structure') > -1:
                    for iatom in range(2*b_atom):
                        data = lines[n+iatom+2].split()
                        tmp = []
                        for icoord in range(3):
                            tmp=tmp+[ float(data[1 + icoord])]
                        coord.append(tmp)
                        for i in range(0,len(coord),2):
                                coord_MD.append(coord[i])
                        for i in range(1,len(coord),2):
                                veloc_MD.append(coord[i])
  #                      atom_coord= copy.deepcopy(coord_md)
    return(coord_MD, veloc_MD)

def read_MD_restart(aims_restart):
        coord_MD=[]
        coord   =[]
        atom_veloc=[]
        tmp = []
        lines = open(aims_restart, 'r').readlines()
        for n, line in enumerate(lines):
                if line.rfind('coord') > -1:
                        tmp=line.split()
                        coord_MD.append(map(float,tmp[-3:]))
                if line.rfind('v_half') > -1:
                        tmp=line.split()
                        atom_veloc.append(map(float, tmp[-3:]))
        coord_MD = np.array(coord_MD)
        coord_MD = coord_MD*bohr2angstrom
        coord_MD = np.reshape(coord_MD, (-1,3))
        atom_veloc = np.array(atom_veloc)
        atom_veloc = np.array(atom_veloc)
        atom_veloc = atom_veloc*bohr2angstrom
        atom_veloc = np.reshape(atom_veloc, (-1,3))
        return coord_MD, atom_veloc

def read_md_results(N_particle, filename, MD_restart):
        results = {}
        converged = read_convergence(filename)
        if not converged:
            os.system('tail -20 ' + filename)
            raise RuntimeError('FHI-aims did not converge!\n' +
                               'The last lines of output are printed above ' +
                               'and should give an indication why.')
        Fre, Ene = read_energy(filename)
        results['free_energy'] = Fre
        results['energy'] = Ene
        H        = read_pseudoH(filename)
#   coord, veloc    = read_geometry(N_particle, filename)
        coord, veloc    = read_MD_restart(MD_restart)
        results['pseudoH']= H
        results['structure']=coord
        results['velocity'] =veloc
        return(results)

def read_scf_results(filename):
        results = {}
        converged = read_convergence(filename)
        if not converged:
            os.system('tail -20 ' + filename)
            raise RuntimeError('FHI-aims did not converge!\n' +
                               'The last lines of output are printed above ' +
                               'and should give an indication why.')
        Fre, Ene = read_energy(filename)
        results['free_energy'] = Fre
        results['energy'] = Ene
        return(results)


def MB_sample_box_mueller(T, m):
## Gaussian distribution from uniform distribution of random numbers in Angstrom/ps 
        bool1 = False
        bool2 = False
        saved = False
        while not bool1:
                var1 = random.random()
                if var1==0:
                        bool1 = False
                else:
                        bool1 = True
        while not bool2:
                var2 = random.random()
                if var2==0:
                        bool2 = False
                else:
                        bool2 = True
        MD_KE_factor = 1.0e12 # this changes from second to picosecond 
        if not saved:
                saved = True

                v = config.c/MD_KE_factor*math.sqrt(config.kB*T/(m*931.49409e6))*math.sqrt(-2.0*math.log(var1))*math.cos(2.0*math.pi*var2)
        else:
                saved = False
                v = config.c/MD_KE_factor*math.sqrt(config.kB*T/(m*931.49409e6))*math.sqrt(-2.0*math.log(var1))*math.sin(2.0*math.pi*var2)
        return v

# distribute the velocities(vx, vy, vz) to a new atom according to MB distribution
def gcmd_velocity(temp):
        v_half = np.zeros((3))
        for j in range(3):
                m = config.mass[1]
                v_half[j] = MB_sample_box_mueller(temp, m)
        return v_half

def remd_rescale_velocity(former_v, v_factor):
    former_v = np.array(former_v)
    after_v  = former_v*v_factor
    return after_v

def MB_sample_box_mueller2(T, m):
## Gaussian distribution from uniform distribution of random numbers    
        bool1 = False
        bool2 = False
        saved = False
        while not bool1:
                var1 = random.random()
                if var1==0:
                        bool1 = False
                else:
                        bool1 = True
        while not bool2:
                var2 = random.random()
                if var2==0:
                        bool2 = False
                else:
                        bool2 = True
        boltzmann_kB = 3.16681535e-6   #for MD, need Boltzmann constant (in Ha/K)
        MD_KE_factor = 1.06657213e-6 # this changes from kinetic energy in MD units (amu*bohr^2/ps^2) to Hartree 
        if not saved:
                saved = True

                v = math.sqrt(boltzmann_kB*T/(MD_KE_factor*m))*math.sqrt(-2.0*math.log(var1))*math.cos(2.0*math.pi*var2)
        else:
                saved = False
                v = math.sqrt(boltzmann_kB*T/(MD_KE_factor*m))*math.sqrt(-2.0*math.log(var1))*math.sin(2.0*math.pi*var2)
        return v

def rewrite_gcmd_restart(struct, temp, rank):
    srank='%.2d' %rank
    finput = open(os.path.join('replica_'+srank, 'aims_MD_restart.dat'),'r')
    noo=len(glob.glob('replica_'+srank+'/aims_MD_restart.dat*'))
    os.system('cp replica_'+srank+'/aims_MD_restart.dat '+'replica_'+srank+'/aims_MD_restart.dat_'  + str(noo))
    lines = finput.readlines()
    indata = {}
    data   = []
    for line in lines:
        data.append(line)
        if line.strip()[0] != '#':
            litem = line.split()
            indata[litem[0].strip().lower()] = litem[1].strip()
        natoms = int(indata['n_atoms'][0])
        if len(struct) != natoms:
            v_half = np.zeros((len(struct),3))
            for i in range(len(struct)):
                for j in range(3):
                    if i < config.n_metal:
                        m = config.mass[0]
                    else:   
                        m = config.mass[1]
                    v_half[i][j] = MB_sample_box_mueller(temp, m)
        v_last = v_half
        struct = np.array(struct)
        struct = struct/0.52917
        r_last = struct
        outfile = open(os.path.join('replica_'+srank,'tmp_MD_restart'), 'a+')
        for i in range(6):
            outfile.write(str(data[i]))
        outfile.write('%10s %10d\n' %('n_atoms', len(struct)))
        for i in range(len(struct)):
                        outfile.write('%10s %20.10E %20.10E %20.10E\n' %('coords', struct[i][0], struct[i][1], struct[i][2]))
        for i in range(len(struct)):
            outfile.write('%10s %20.10E %20.10E %20.10E\n' %('v_half', v_half[i][0], v_half[i][1], v_half[i][2]))
        for i in range(len(struct)):
            outfile.write('%10s %20.10E %20.10E %20.10E\n' %('v_last', v_last[i][0], v_last[i][1], v_last[i][2]))
        for i in range(len(struct)):
                        outfile.write('%10s %20.10E %20.10E %20.10E\n' %('r_last', r_last[i][0], r_last[i][1], r_last[i][2]))
        for i in range(7+natoms*4, len(data),1):
            outfile.write(str(data[i]))
        os.system('mv replica_'+srank+'/tmp_MD_restart ' + 'replica_'+srank+'/aims_MD_restart.dat')

def rewrite_remd_restart(v_suffix, rank):
    srank='%.2d' %rank
    finput = open(os.path.join('replica_'+srank,'aims_MD_restart.dat'),'r')
    noo=len(glob.glob('replica_'+srank+'/aims_MD_restart.dat*'))
    os.system('cp replica_'+srank+'/aims_MD_restart.dat '+'replica_'+srank+'/aims_MD_restart.dat_'  + str(noo))
    lines = finput.readlines()
    indata = {}
    data   = []
    ddata  = []
    for line in lines:
        ddata.append(line)
        if line.strip()[0] != '#':
                    litem = line.split()
                    data.append(litem)
                    indata[litem[0].strip().lower()] = litem[1].strip()
    natoms = int(indata['n_atoms'][0])
    outfile = open(os.path.join('replica_'+srank,'tmp_MD_restart'), 'a+')
    for i in range(7+natoms):
        outfile.write(str(ddata[i]))
    for i in range(1+natoms, 1+natoms*2, 1):
        outfile.write('%10s %20.10E %20.10E %20.10E\n' %(data[i][0], float(data[i][1])*v_suffix, float(data[i][2])*v_suffix, float(data[i][3])*v_suffix))
    for i in range(1+natoms*2, 1+natoms*3, 1):
        outfile.write('%10s %20.10E %20.10E %20.10E\n' %(data[i][0], float(data[i][1])*v_suffix, float(data[i][2])*v_suffix, float(data[i][3])*v_suffix))
    for i in range(7+natoms*3, len(ddata), 1):
        outfile.write(str(ddata[i]))    
    os.system('mv replica_'+srank+'/tmp_MD_restart ' + 'replica_'+srank+'/aims_MD_restart.dat')
            


##test##
#atomic=[[0,0,0],[0,1.0,0],[1.0,0.0,0.0]]   
#atomic=[[0,0,0],[0,1.0,0]]
#T=300.0
#rewrite_md_restart(atomic, T)
#rewrite_remd_restart(2.0)
